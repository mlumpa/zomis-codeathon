$(document).ready(function () {
	$(".tablesorter").tablesorter({
        headers: {
            0: {
                sorter: false
            }
        }
    });
	
	var set_summary_counts = function () {
		$.getJSON( "/dashboard", function( data ) {
			  var items = [];
			  $.each( data, function( key, val ) {
				  if(key === "summary"){
					  $('#org_count_label').text(val['orgs']);
					  $('#workforce_count_label').text(val['workforce']);
					  $('#user_count_label').text(val['user']);
					  $('#guardian_count_label').text(val['guardian']);
					  $('#children_count_label').text(val['children']);
				  }
				 if(key === "workforce_type"){
					 var radar_values = val["values"];
					 var radar_labels = val["labels"];
					 
					 var radarChartData = {
						        labels : radar_labels,
						        datasets : [
						            {
						            	fillColor: "rgba(151,187,205,0.2)",
						                strokeColor: "rgba(151,187,205,1)",
						                pointColor: "rgba(151,187,205,1)",
						                pointStrokeColor: "#fff",
						                pointHighlightFill: "#fff",
						                pointHighlightStroke: "rgba(151,187,205,1)",
						                data : radar_values
						            }]
						    	}

					 var ctx = document.getElementById("workforce_user_radar").getContext("2d");
					 ctx.canvas.width = 400;
					 ctx.canvas.height = 400;
					 var myRadarChart = new Chart(ctx).Radar(radarChartData ,{scaleShowLabels : true, pointLabelFontSize : 10, datasetFill : true, scaleBeginAtZero : true, datasetStroke : true, datasetStrokeWidth : 2, showToolTips:true});
				 }
				if(key === "orgs_type"){
					var contents = [];
					$.each( val, function( org_title, data ){
						var percentage = Math.round((data[0]/data[1])*100);
						var progresbar= '<div class="progress" style="margin-bottom:0px"><div class="progress-bar" role="progressbar" aria-valuenow="'+percentage+'" aria-valuemin="0" aria-valuemax="100"  style="width:'+percentage+'%;"></div></div>';
						contents.push('<tr><td>'+progresbar+''+org_title+'</td><td style="text-align:right">'+data[0]+'</td><td style="text-align:right">'+percentage+'%</td></tr>');
						//$('#orgs_progress_bar > tbody:last').append('<tr><td>'+progresbar+''+org_title+'</td><td style="text-align:right">'+data[0]+'</td><td style="text-align:right">'+percentage+'%</td></tr>');
					});
					$('#orgs_progress_bar > tbody:last').append(contents.join(' ')).trigger("update");
					//$("#orgs_progress_bar").trigger("update");
							
				}
			   if(key === "registries"){
				   var contents = [];
				   $.each( val, function( month, rows ){
					   contents.push('<tr><td>'+month+'</td><td>'+rows['org_new']+'</td><td>'+rows['org_updated']+'</td><td>'+rows['workforce_new']+'</td><td>'+rows['workforce_updated']+'</td><td>'+rows['beneficiary_new']+'</td><td>'+rows['beneficiary_updated']+'</td></tr>');
					   //$('#registries_new_updated > tbody:last').append('<tr><td>'+month+'</td><td>'+rows['org_new']+'</td><td>'+rows['org_updated']+'</td><td>'+rows['workforce_new']+'</td><td>'+rows['workforce_updated']+'</td><td>'+rows['beneficiary_new']+'</td><td>'+rows['beneficiary_updated']+'</td></tr>');
					});
				   $('#registries_new_updated > tbody:last').append(contents.join(' '));
			   }
			  }
		  )});
		$(this).tab('show');
		};

	set_summary_counts();
	
	$('#children_tab').click(function (e) {
		$.getJSON( "/dashboard_children", function( data ){
			$.each( data, function( key, val ) {
				if(key === "children_age_range"){
					$('#register_children_chart').highcharts({
				        chart: {
				            type: 'column'
				        },
				        title: {
				            text: 'Registered children by age'
				        },
				        xAxis: {
				            categories: val['categories']
				        },
				        yAxis: {
				            min: 0,
				            title: {
				                text: 'Number of children'
				            },
				            stackLabels: {
				                enabled: true,
				                style: {
				                    fontWeight: 'bold',
				                    color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
				                }
				            }
				        },
				        legend: {
				            align: 'right',
				            x: -70,
				            verticalAlign: 'top',
				            y: 20,
				            floating: true,
				            backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColorSolid) || 'white',
				            borderColor: '#CCC',
				            borderWidth: 1,
				            shadow: false
				        },
				        tooltip: {
				            formatter: function() {
				                return '<b>'+ this.x +'</b><br/>'+
				                    this.series.name +': '+ this.y +'<br/>'+
				                    'Total: '+ this.point.stackTotal;
				            }
				        },
				        plotOptions: {
				            column: {
				                stacking: 'normal',
				                dataLabels: {
				                    enabled: true,
				                    color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white'
				                }
				            }
				        },
				        series: [{
						            name: 'With birth certificate number',
						            data: val['values_birth_reg']
						        }, {
						            name: 'Without birth certificate number',
						            data: val['values_no_birth_reg']
						        }
						       ]
				    });

				}
				if(key == "registered_by_province"){
					$('#children_by_province').highcharts({
				        chart: {
				            plotBackgroundColor: null,
				            plotBorderWidth: null,
				            plotShadow: false
				        },
				        title: {
				            text: 'Number of children by province'
				        },
				        tooltip: {
				            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
				        },
				        plotOptions: {
				            pie: {
				                allowPointSelect: true,
				                cursor: 'pointer',
				                dataLabels: {
				                    enabled: false
				                },
				                showInLegend: true
				            }
				        },
				        series: [{
				            type: 'pie',
				            name: 'Children Province Distribution',
				            data: val
				            
				        }]
				    });
				}
				if(key === "top_adverse_conds"){
					var contents = [];
					$.each( val, function( title, data ){
						var percentage = Math.round((data[0]/data[1])*100);
						var progresbar= '<div class="progress" style="margin-bottom:0px"><div class="progress-bar" role="progressbar" aria-valuenow="'+percentage+'" aria-valuemin="0" aria-valuemax="100"  style="width:'+percentage+'%;"></div></div>';
						contents.push('<tr><td>'+progresbar+''+title+'</td><td style="text-align:right">'+data[0]+'</td><td style="text-align:right">'+percentage+'%</td></tr>');
						//$('#top_conds_progress_bar > tbody:last').append('<tr><td>'+progresbar+''+title+'</td><td style="text-align:right">'+data[0]+'</td><td style="text-align:right">'+percentage+'%</td></tr>');
					});
					$('#top_conds_progress_bar > tbody:last').append(contents.join(' ')).trigger("update");
					//$("#top_conds_progress_bar").trigger("update");
				}
				
			});
			
		});
	  $(this).tab('show');
	});
});