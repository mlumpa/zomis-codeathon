   
'''
Created on Sep 18, 2014

@author: GLyoko
'''
from django import forms
from django.core.exceptions import ValidationError
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Fieldset, ButtonHolder, Submit, Button, HTML, MultiField, Div, Field
from crispy_forms.bootstrap import FormActions, AppendedText
from ovc_main.utils import fields_list_provider as workforceFields, validators as validate_helper
from functools import partial
from models import SetupGeorgraphy
from ovc_main.models import RegPerson
#from pip._vendor.colorama.ansi import Style
DateInput = partial(forms.DateInput, {'class': 'datepicker'})
from ovc_main.utils.auth_access_control_util import del_form_field, fields_to_show,filter_fields_for_display
#from workforce_registration import get_workforce_permission_fields
from django.conf import settings


roles_and_fields_to_show = {}
fields_by_perms = {'rel_org':
                        {        
                        'auth.update wkf general rel':['workforce_type', 'man_number', 'ts_number', 'sign_number','steps_ovc_number','direct_services','org_unit','primary_parent_org_unit',
                                                    'workforce_type_change_date','parent_org_change_date','work_locations_change_date','nrc','date_of_death','edit_mode_hidden','workforce_id', 'districts', 'wards', 'communities', 'wfc_system_id', 'org_data_hidden']
                        },
                
                    'own':
                        {
                        'auth.update wkf contact own':['designated_phone_number', 'other_mobile_number', 'email_address', 'physical_address','edit_mode_hidden','workforce_id', 'wfc_system_id']
                        },
                    'other':
                        {
                        'auth.update wkf special':['first_name', 'last_name', 'other_names', 'sex', 'date_of_birth','nrc','edit_mode_hidden','workforce_id', 'wfc_system_id'],
                        'auth.update wkf contact':['designated_phone_number', 'other_mobile_number', 'email_address', 'physical_address','edit_mode_hidden','workforce_id', 'wfc_system_id'],
                        'auth.update wkf general':['workforce_type', 'man_number', 'ts_number', 'sign_number','steps_ovc_number','direct_services','org_unit','primary_parent_org_unit','workforce_type_change_date',
                                                    'parent_org_change_date','work_locations_change_date','nrc','date_of_death','edit_mode_hidden','workforce_id', 'districts', 'wards', 'communities', 'wfc_system_id', 'org_data_hidden']
                        }
                    }


class WorkforceRegistrationForm(forms.Form):
    is_update_page = False
    wfc_id = ''
    workforce_id = forms.CharField(label='Workforce member ID: ', required=False, initial = wfc_id, widget=forms.HiddenInput())
    nrc = forms.RegexField(regex=r'^\b\d{6}\/\b\d{2}\/\b[1-3]{1}$',error_message = ("NRC number must be entered in the format 987654/32/1"),label='NRC: ', required=False)
    wfc_system_id =  forms.CharField(label='Person_id: ', required=False, widget=forms.HiddenInput())
    #table table-hover table-condensed
    
    first_name = forms.CharField(label='First Name: ')
    last_name = forms.CharField(label='Surname: ')
    other_names = forms.CharField(label='Other name(s): ', required=False)
    sex = forms.ChoiceField(label='Sex: ',choices=workforceFields.get_sex_list())
    date_of_birth = forms.DateField(widget=DateInput(), input_formats=validate_helper.date_input_formats_tpl,validators=[validate_helper.validate_date_of_birth])#,input_formats=validate_helper.date_input_formats)
        
    workforce_type = forms.ChoiceField(label='Workforce member type: ',choices=workforceFields.get_Workforce_Type())
    man_number = forms.CharField(label='MAN number: ', required=False)
    ts_number = forms.CharField(label='if teacher, TS number: ', required=False)
    sign_number = forms.CharField(label='If police officer, Sign number: ', required=False)
    direct_services = forms.ChoiceField(widget=forms.RadioSelect, choices=workforceFields.get_YesNo_Enums(),label='Does this person provide services directly to children?')#(label='Do you provide services directly to children?: ',wid)

    org_unit = forms.CharField(required=False, label='Organization unit: ')
    primary_parent_org_unit = forms.ChoiceField(required=False, widget=forms.RadioSelect, choices=workforceFields.get_YesNo_Enums(),label='Primary parent organisation unit: ')
    workforce_reg_assistant = forms.ChoiceField(required=False, widget=forms.RadioSelect, choices=workforceFields.get_YesNo_Enums(),label='Workforce registration assistant for this organization: ')
    
    designated_phone_number = forms.RegexField(required=False,label='Designated mobile number for sending and receiving SMSes: ', regex=validate_helper.mobile_phone_number_regex, error_message = ("Phone number must be entered in the format: '+260977123456' or '0977123456'."))
    other_mobile_number = forms.RegexField(required=False,label='Other mobile number: ',regex=validate_helper.mobile_phone_number_regex, error_message = ("Phone number must be entered in the format: '+260977123456' or '0977123456'."))
    email_address = forms.EmailField(required=False,label='Email: ')
    physical_address = forms.CharField(required=False,widget=forms.Textarea(attrs={'rows': 4}), label='Physical address: ')

    org_data_hidden = forms.CharField(label='',required=True)
    steps_ovc_number = forms.CharField(label = 'STEPS OVC number: ', required=False)
   # wfc_system_id = forms.CharField()
    #TODO add validations
    workforce_type_change_date = forms.DateField(widget=DateInput(), input_formats=validate_helper.date_input_formats_tpl, required=False,label='')#,input_formats=validate_helper.date_input_formats)
    parent_org_change_date = forms.DateField(widget=DateInput(), input_formats=validate_helper.date_input_formats_tpl, required=False,label = '')#,input_formats=validate_helper.date_input_formats)
    work_locations_change_date = forms.DateField(widget=DateInput(), input_formats=validate_helper.date_input_formats_tpl, required=False, label = '')#,input_formats=validate_helper.date_input_formats)
    date_of_death = forms.DateField(widget=DateInput(), input_formats=validate_helper.date_input_formats_tpl, required=False,validators=[validate_helper.deny_future_dates])#,input_formats=validate_helper.date_input_formats)
    
    edit_mode_hidden = forms.CharField(required=False)
            
    def __init__(self, user, *args, **kwargs):
        self.user = user
        is_update_page = False
        do_display_status = 'display:None'
        hide_status = 'text'
        unhide_status = 'visibility:hidden'
        dont_display_status = ''
        actionurl = '/workforce/new_workforce/'
        header_text = """<div class="note note-info"><h4><strong>Register New Workforce Member ("""+settings.APP_NAME+""" form 2a) / Register user</strong>
                        </h4><h5><p>This form is used to register workforce members who provide services directly to vulnerable children, as wells as other users of this system</h5></div>"""
        submitButtonText = 'Save'      
        
        
        _districts_options_list = ''
        self.districts = []
        self.wards = []
        self.communities = []

        if args and 'districts' in args[0]:
            self.districts = get_list_from_dic_or_querydict(args[0], 'districts')
            self.districts = convert_to_int_array(self.districts)
            _districts_options_list = workforceFields.geo_control_select_list('district', self.districts, setdisabled=False)
        else:
            _districts_options_list = workforceFields.geo_control_select_list('district')   
        
        _ward_option_list = ''
        if args and 'wards' in args[0]:
            self.wards = get_list_from_dic_or_querydict(args[0], 'wards')
            self.wards = convert_to_int_array(self.wards)
            if self.districts:
                _ward_option_list = workforceFields.geo_control_select_list('ward', self.wards, grandparentfilter=self.districts, setdisabled=False) 
        elif self.districts:
            _ward_option_list = workforceFields.geo_control_select_list('ward', grandparentfilter=self.districts)  
        
        #print wards, '1in communities wards to print'
        _community_option_list = ''
        if args and 'communities' in args[0]:
            self.communities = get_list_from_dic_or_querydict(args[0], 'communities')
            self.communities = convert_to_int_array(self.communities)
            if self.communities:
                _community_option_list = workforceFields.community_control_populate(self.communities, self.wards)
            else:
                _community_option_list = workforceFields.community_control_populate(parent_filter=self.wards)
        elif self.wards:
            _community_option_list = workforceFields.community_control_populate(parent_filter=self.wards)
        
        self.failed_save = False
        self.national_id = None
        self.workforce_related_to_user = False
        self.workforce_is_logged_in_user = False
        if len(args) > 0:    
            if 'nrc' in args[0]:            
                self.national_id = args[0]['nrc']
            if 'is_related_to_user' in args[0]:            
                self.workforce_related_to_user = args[0]['is_related_to_user']
            if 'is_logged_in_user' in args[0]:            
                self.workforce_is_logged_in_user = args[0]['is_logged_in_user']
            
        wfc_system_id=None
        if len(args) > 0 and 'wfc_system_id' in args[0]:
            wfc_system_id = args[0]['wfc_system_id']
        primary_org_id=None
        if len(args) > 0 and 'primary_org_id' in args[0]:
            primary_org_id = args[0]['primary_org_id']
        
        is_from_submit=None
        if len(args) > 0 and 'submit' in args[0]:
            is_from_submit = args[0]['submit']
            
        if 'workforce_id' in args:
            wfc_id = args.pop('workforce_id')
        self.geo_control_mandatory = ''
        if 'is_update_page' in kwargs:
            self.is_update_page = kwargs.pop('is_update_page')
            if self.is_update_page:
                do_display_status = ''
                dont_display_status = 'display:None'
                hide_status = 'visibility:hidden'
                unhide_status = ''
                header_text = """<div class="note note-info"><h4><strong>File update workforce member Update("""+settings.APP_NAME+""" form 2a) / Update user</strong></h4><h5><p>This form is used to update a workforce member</h5></div>"""
                actionurl = '/workforce/update_wfc/'
                submitButtonText = 'Update'
                self.edit_mode_hidden = forms.CharField()

        super(WorkforceRegistrationForm, self).__init__(*args, **kwargs)
                        
        #self.fields['date_of_birth'].widget.attrs['readonly'] = True
        #self.fields['date_of_death'].widget.attrs['readonly'] = True
        
        self.helper = FormHelper()
        self.helper.form_class = 'form-horizontal'
        self.helper.label_class = 'col-md-3'
        self.helper.attrs = {'padding-top':'3px'}
        self.helper.field_class = 'col-md-6'
        self.helper.form_action = actionurl
        self.helper.layout = Layout(
            MultiField(
                        header_text,
                        Div(
                                Div(
                                    HTML('<p/>'),
                                    HTML("""<div id="div_id_workforce_id" class="form-group mandatory" style=\""""+do_display_status+"""\">
                                            <label for="id_workforce_id" class="control-label col-md-3 mandatory">Workforce member ID:</label>
                                            <div class="mandatory">
                                              <p class="form-control-static col-md-6 mandatory"><strong>{{workforce.workforce_id}}</strong></p>
                                            </div>
                                            
                                        </div>"""),
                                    'workforce_id',
                                    Field(Div('wfc_system_id', style='display:inline'), css_class='form-inline mandatory'),
                                    'nrc',
                                    css_class = 'panel-body pan',
                                    ),
                                css_class='panel panel-grey',
                            ),
                        Div(
                                Div(HTML('<input id="id_edit_mode" class="mandatory" type="radio" name="edit_mode" value="1" {{select_fields_radio}}> Update workforce/user details'
                                    ),
                                    css_class = 'panel-heading clearfix'
                                    ),
                                css_class='panel panel-green',
                                style = do_display_status,
                            ),
                        Div(
                                Div(HTML('Personal Details'),
                                    css_class = 'panel-heading'
                                    ),
                                Div(
                                    HTML('<p/>'),
                                    'first_name',
                                    'last_name',
                                    AppendedText('other_names','<span data-toggle="tooltip" title="Any other names which are either in official documents or are commonly used to refer to the person" data-placement="bottom"><i class="fa fa-info-circle"></i></span>'),
                                    'sex',
                                    AppendedText('date_of_birth','<span data-toggle="tooltip" title="If unknown, put estimate" data-placement="bottom"><i class="fa fa-info-circle"></i></span>'),
                                    css_class = 'panel-body pan',
                                    ),
                                css_class='panel panel-pink mandatory',
                            ),
                        Div(
                                Div(HTML('<span="mandatory">Workforce Type</span>'),
                                        Div(
                                            Div(
                                                HTML('<span class="workforce_type_change_date">Workforce type change date: </span>'),
                                                Field(Div('workforce_type_change_date', style='display:inline'), css_class='form-inline'),
                                                css_class = 'form-inline datelabel',
                                                style = do_display_status,
                                            ),
                                        css_class= 'toolbars'
                                        ),
                                    css_class = 'panel-heading deepsearch'
                                    ),
                                
                                Div(
                                    HTML('<p/>'),
                                    'workforce_type',
                                    'man_number',
                                    'ts_number',
                                    'sign_number',
                                    'steps_ovc_number',
                                    Field('direct_services', style='display:inline', css_class='form-inline'),
                                    css_class = 'panel-body pan'
                                    ),
                                css_class='panel panel-grey',
                        
                            ),
                        Div(
                                Div(HTML('<span class="mandatory">Parent Organisational Unit(s)</span>'),
                                Div(
                                    Div(
                                        HTML('<span class="parent_org_change_date">Parent unit(s) change date: '),
                                        Field(Div('parent_org_change_date', style='display:inline'), css_class='form-inline'),
                                        css_class = 'form-inline datelabel',
                                        style = do_display_status,
                                        ),
                                    css_class= 'toolbars'
                                    ),
                                    css_class = 'panel-heading deepsearch'
                                    ),
                                
                                Div(
                                    HTML('<p/>'),
                                    Field('org_unit', style="display:inline", css_class="autocompleteovc form-inline", id="id_org_unit"),
                                    'primary_parent_org_unit',
                                    Field(Div('workforce_reg_assistant', style=dont_display_status), css_class='form-inline'),
                                    Field(Div('org_data_hidden', style='display:None'), css_class='form-inline mandatory'),                                    
                                    HTML("<label id=\"valid_org_unit\" style=\"color:red;margin-left:50px;display:None\"><strong>  </strong></label>"),
                                    css_class = 'panel-body pan'
                                    ),
                                Div(
                                    HTML("<br><a type=\"button\" onclick=\"addOrgUnitData()\" class=\"btn btn-info org_data_hidden\"><i class=\"fa fa-plus\"></i> Add</a><br><br>"),
                                    style='margin-left:41%'
                                    ),
                                Div(
                                    HTML("""
                                    <!--div class="org_data_hidden" id="div_id_org_unit_table" style=\""""+hide_status+"""\"-->
                                    <table id="orgTable"  class="table table-hover table-condensed">
                                        <thead>
                                        <tr>
                                            <th style="width:1%"></th>
                                            <th>Org Unit</th>
                                            <th>Org unit ID</th>
                                            <th>Primary parent org unit</th>
                                            <th  id = "id_reg_assistant_col" style=\""""+hide_status+"""\">Registration assistant for the organization</th>
                                            <th>Remove</th>
                                        </tr>
                                        </thead>
                                        <tbody>

                                        </tbody>
                                    </table>
                                    <!--/div-->
                                    <input id="org_unit_id_hidden" class="org_data_hidden" type="hidden">
                                    <input id="org_unit_name_hidden" class="org_data_hidden" type="hidden">                              
                                    
                                    <script>
                                    $( "#id_date_of_birth" ).datepicker( "option", "dateFormat", 'd MM, yy' );
                                    
                                    function addOrgUnitData() {
                                        //http://stackoverflow.com/questions/171027/add-table-row-in-jquery
                                                                    
                                        var primary_index = $('input:radio[name=primary_parent_org_unit]:checked').val()
                                        var primary_org_unit_index = primary_index;
         
                                        if(!primaryOrgCountOk(primary_index)){
                                            return;
                                        }
            
                                        if(!validateOrgUnitInput()){
                                            return;
                                        }                                        

                                        var primary_org_unit = '';
                                        $("#valid_org_unit").text("");
                                        $("#valid_org_unit").css('display','None');
                                        switch(primary_org_unit_index){
                                            case '1':
                                                primary_org_unit = 'Yes';
                                                break;
                                            case '2':
                                                primary_org_unit = 'No';
                                                break;
                                            default:
                                                $("#valid_org_unit").text("Please select an option for 'Primary parent organisation unit'");
                                                $("#valid_org_unit").show();
                                                return;
                                        }
                                        
                                        var is_workforce_reg_assistant_index = $('input:radio[name=workforce_reg_assistant]:checked').val();
                                        var is_workforce_reg_assistant = '';
                                        switch(is_workforce_reg_assistant_index){
                                            case '1':
                                                is_workforce_reg_assistant = 'Yes';
                                                break;
                                            case '2':
                                                is_workforce_reg_assistant = 'No';
                                                break;
                                        }
                                    
                                        $("#orgTable").find('tbody')
                                            .append($('<tr>')
                                                .append($('<td id="0">')
                                                    .append($('<label>')
                                                        .text('')
                                                        .css('visibility','hidden')
                                                        .css('width','1%')
                                                    )
                                                )
                                                .append($('<td id="1">')
                                                    .append($('<label>')
                                                        .text($("#org_unit_name_hidden").val())
                                                    )
                                                )
                                                .append($('<td id="2">')
                                                    .append($('<label>')
                                                        .text($("#org_unit_id_hidden").val())
                                                    )
                                                )
                                                .append($('<td id="3">')
                                                    .append($('<label>')
                                                        .text(primary_org_unit)
                                                    )
                                                )
                                                .append($('<td id="4" style=\""""+hide_status+"""\">')
                                                    .append($('<label>')
                                                        .text(is_workforce_reg_assistant)
                                                    )
                                                )
                                                .append($("<td id='5'> <button onclick='myDeleteFunction()' class='removebutton'><i class='fa fa-trash fa-2'></i></button></td>"))
                                            );
                                            
                                            updateTableData();
                                            clearForm();
                                            $( "#primary_parent_org_unit" ).prop( "checked", false );
                                        }
                                        
					function primaryOrgCountOk(primary_org_selected){
                                            var count = 0;
                                            var toReturn = true;
                                            $("#valid_org_unit").text("");
                                            $("#valid_org_unit").css('display','None');
                                            $("#orgTable tr").each(function(i, v){
                                                $(this).children('td').each(function(ii, vv){
                                                    if($(this).attr("id") == '3'){
                                                        if($(this).text() == 'Yes'){
                                                            count++;
                                                        }
                                                        if(count>=1 && primary_org_selected == 1){//Yes previously selected
                                                            $("#valid_org_unit").text("A primary organisation unit has already been set for this person. Only one primary .org. unit can be set per person.");
                                                            $("#valid_org_unit").show();
                                                            toReturn = false;
                                                        }
                                                    }
                                                    if($(this).attr("id") == '2'){
                                                        if($(this).text() == $("#org_unit_id_hidden").val()){
                                                            $("#valid_org_unit").text("This organisation unit has already been added for this person.");
                                                            $("#valid_org_unit").show();
                                                            toReturn = false;
                                                        } 
                                                    }													
                                                });
                                            });
                                            return toReturn;
                                        }
                                        
                                        function validateOrgUnit(orgunit) { 
                                          // http://stackoverflow.com/a/46181/11236
                                            var re = /^U\d{6}\,.*$/;
                                            return re.test(orgunit);
                                        }

                                        function validateOrgUnitInput(){
                                            $("#valid_org_unit").text("");
                                            $("#valid_org_unit").css('display','None');
                                            var org_unit_text = $("#id_org_unit").val();
                                            if (!validateOrgUnit(org_unit_text)) {
                                                $("#valid_org_unit").text("Please select a valid organisation unit from the drop down list as you type the name of the organisation unit. ");
                                                $("#valid_org_unit").show();
                                                return false;
                                            } 
                                            return true;
                                        }
                                        
                                        function updateTableData(){
                                        if($( "#id_org_data_hidden" ).text() != '')
                                            {
                                                return;
                                            }
                                            //var header = Array();    
                                            //$("table tr th").each(function(i, v){
                                                    //header[i] = $(this).text();
                                            //})

                                            var data = Array();
                                            $("#orgTable tr").each(function(i, v){
                                                data[i] = Array();
                                                $(this).children('td').each(function(ii, vv){
                                                    if($(this).attr("id") == '1'){
                                                        data[i][ii] = 'org_name:'+$(this).text();
                                                    }else 
                                                    if($(this).attr("id") == '2'){
                                                        data[i][ii] = 'org_id:'+$(this).text();
                                                    }else if($(this).attr("id") == '3'){
                                                        data[i][ii] = 'primary_org:'+$(this).text();
                                                    }else if($(this).attr("id") == '4'){
                                                        data[i][ii] = 'wra:'+$(this).text();
                                                    }
                                                    else{                                                    
                                                        //data[i][ii] = $(this).text();
                                                    }
                                                
                                                }); 
                                            })
                                            
                                            $( "#id_org_data_hidden" ).val(data);
                                        }
                                        
                                        function clearForm(){
                                            $('input[name="org_unit"]').val('');
                                            $('input[id="org_unit_id_hidden"]').val('');
                                            $('input[id="org_unit_name_hidden"]').val('');
                                            //http://fronteed.com/iCheck/ so cool :)
                                            $( 'input[id="id_primary_parent_org_unit_1"]').iCheck('uncheck');
                                            $( 'input[id="id_workforce_reg_assistant_1"]').iCheck('uncheck');
                                            $( 'input[id="id_primary_parent_org_unit_2"]').iCheck('uncheck');
                                            $( 'input[id="id_workforce_reg_assistant_2"]').iCheck('uncheck');                                                       
                                        }   
                                        
                                        function myDeleteFunction(){
                                            $(document).on('click', 'button.removebutton', function () { 
                                               $(this).closest('tr').remove();
                                               updateTableData();
                                               return false;
                                           });
                                        }
                                        
                                        function updateFinale(){
                                            updateTableData();
                                            checkUpdateMode();
                                        }
                                        
                                        function checkUpdateMode(){
                                            var edit_mode = $('input:radio[name=edit_mode]:checked').val();
                                            $( "#id_edit_mode_hidden" ).val(edit_mode);  
                                            
                                        }
                                        
                                    </script>
                                                                        
                                    """),
                                    css_class = 'panel-body '
                                    ),
                                css_class='panel panel-pink',
                        
                            ),
                        Div(
                                Div(HTML('<span class="mandatory">Where do you work?</span>'),
                                    Div(
                                        Div(
                                            HTML('<span class="work_locations_change_date">Work location(s) change date: </span>'),
                                            Field(Div('work_locations_change_date', style='display:inline'), css_class='form-inline'),
                                            css_class = 'form-inline datelabel datelabel',
                                            style = do_display_status,
                                            ),
                                        css_class= 'toolbars'
                                        ),
                                    css_class = 'panel-heading deepsearch'
                                    ),
                                Div(
                                    HTML('<p/>'),
                                    Div(
                                        HTML("""
                                             
                                             <label class="control-label col-md-3 """+self.geo_control_mandatory+"""">Districts:</label>
                                             <div class="col-md-6 """+self.geo_control_mandatory+"""" id="multipledistricts">
                                                 <select id="districts" name="districts" multiple="multiple" ismultiselect="True">"""+
                                                    _districts_options_list+"""
                                                </select>
                                            </div>
                                             
                                             """),
                                        css_class = 'form-group form-horizontal form-bordered',
                                        css_id = 'workforce_district_location'
                                        ),
                                     Div(
                                        HTML("""
                                             
                                             <label class="control-label col-md-3 """+self.geo_control_mandatory+"""">Wards:</label>
                                             <div class="col-md-6 """+self.geo_control_mandatory+"""">
                                                 <select id="wards" multiple="multiple" name="wards" ismultiselect="True">
                                                    """+_ward_option_list+"""
                                                </select>
                                            </div>
                                             
                                             """),
                                        css_class = 'form-group form-horizontal form-bordered',
                                        css_id = 'workforce_ward_location'
                                        ),
                                    Div(
                                        HTML("""
                                             
                                             <label class="control-label col-md-3 """+self.geo_control_mandatory+"""">Communities:</label>
                                             <div class="col-md-6 """+self.geo_control_mandatory+"""">
                                                 <select id="communities" multiple="multiple" name="communities">
                                                    """+_community_option_list+"""
                                                </select>
                                            </div>
                                             
                                             """),
                                        css_class = 'form-group form-horizontal form-bordered',
                                        css_id = 'workforce_community_location'
                                        ),
                                    css_class = 'panel-body pan'
                                    ),
                                    css_class='panel panel-grey',
                        
                            ),
                            Div(
                                Div(HTML('Contact Details'),
                                    css_class = 'panel-heading'
                                    ),
                                
                                Div(
                                    HTML('<p/>'),
                                    AppendedText('designated_phone_number','<span data-toggle="tooltip" title="This must be filled if the person is to interact with '+settings.APP_NAME+' via SMS" data-placement="bottom"><i class="fa fa-info-circle"></i></span>'),
                                    'other_mobile_number',
                                    'email_address',
                                    'physical_address',
                                    css_class = 'panel-body pan'
                                    ),
                                css_class='panel panel-pink',
                        
                            ),
                            Div(
                                Div(HTML('<input id="id_edit_mode" class="mandatory" type="radio" name="edit_mode" value="2" {{select_death_radio}}> Died'),
                                    css_class = 'panel-heading clearfix',
                                    style = do_display_status,
                                     ),
                                Div(
                                    HTML('<p/>'),
                                    Field(Div('date_of_death', style='visibility:text'), css_class='form-inline mandatory'),
                                    Field(Div('edit_mode_hidden', style='display:None'), css_class='form-inline mandatory'),
                                    css_class = 'panel-body pan'
                                    ),
                                css_class='panel panel-green',
                                style = do_display_status,
                            ),
                            Div(
                                Div(HTML('<input id="id_edit_mode" class="mandatory" type="radio" name="edit_mode"  value="3"/> Workforce member / user never existed (was a registration mistake or a duplicate)'),
                                    css_class = 'panel-heading clearfix',
                                    style = do_display_status,
                                    ),
                                css_class='panel panel-green',
                            ),
                            Div(
                                ButtonHolder(
                                    HTML("<Button onclick=\"updateFinale()\" type=\"submit\" name=\"submit\" style=\"margin-right:4px\" class=\"btn btn-primary\"><i class=\"fa fa-floppy-o\"></i> "+submitButtonText+"</Button>"),
                                    HTML("""<a type=\"button\" href=\"/workforce/wfcsearch/\"class=\"btn btn-success\" ><i class="fa fa-times-circle-o"></i> Cancel</a>"""),
                                ),
                                style='margin-left:41%'
                            ),
            )
        )
        
        #if self.is_update_page:
        #    if self.user:
        #        if user.has_perm('auth.update wkf general') or user.has_perm('auth.update wkf general rel'):
        #            self.geo_control_mandatory = 'mandatory'
        #    #print len(roles_and_fields_to_show) == 0,'len(roles_and_fields_to_show) == 0'
        #    if len(roles_and_fields_to_show) == 0:
        #        compile_permissions(self.user,self.national_id,self.workforce_related_to_user,self.workforce_is_logged_in_user,roles_and_fields_to_show)
        #    #print roles_and_fields_to_show,'roles_and_fields_to_show in form 1'
        #    layout = self.helper.layout
        #    searchitems = fields_to_show(self.user, roles_and_fields_to_show['Workforce Update'])
        #    #print searchitems,'searchitems'
        #    if searchitems:
        #        del_form_field(layout, searchitems)
        #        self.helper.layout = layout
        
        if self.is_update_page:
            if self.user:
                if user.has_perm('auth.update wkf general') or user.has_perm('auth.update wkf general rel'):
                    self.geo_control_mandatory = 'mandatory'
            #print len(roles_and_fields_to_show) == 0,'len(roles_and_fields_to_show) == 0'
            if len(roles_and_fields_to_show) == 0:
                compile_permissions(self.user,self.national_id,self.workforce_related_to_user,self.workforce_is_logged_in_user,roles_and_fields_to_show)
            #print roles_and_fields_to_show,'roles_and_fields_to_show in form 1'
            layout = self.helper.layout
            searchitems = filter_fields_for_display(self.user, fields_by_perms, owner_id=wfc_system_id, organisation_id=primary_org_id)
            
            if args and 'nrc' in args[0] and 'submit' in args[0]:
                pass
                
            elif not((user.has_perm('auth.update wkf special') and self.national_id) or 
                       ((user.has_perm('auth.update wkf general') or user.has_perm('auth.update wkf general rel')) 
                            and not self.national_id)):
                if 'nrc' in searchitems:
                    searchitems.pop(searchitems.index('nrc'))
                
            if searchitems:
                del_form_field(layout, searchitems)
                self.helper.layout = layout
            
    def clean(self):
        #print self.cleaned_data.get('org_data_hidden'), 'worforce forms hidden'
        cleaned_data = super(WorkforceRegistrationForm, self).clean()
        if (cleaned_data.get('org_data_hidden') != None):
            value = cleaned_data.get('org_data_hidden')
            if value:
                no_of_primary_org_units = 0
                #,,org_id:U0000018,primary_org:Yes,wra:No,,org_id:U0000034,primary_org:No,wra:No
                orgs = value.split(',,')
                #Check how many primary org units are registered
                for org in orgs:
                    attrs = org.split(',')
                    for attr in attrs:
                        kvp = attr.split(':')
                        if len(kvp) > 1:
                            if kvp[0] == 'primary_org' and kvp[1] == 'Yes':
                                no_of_primary_org_units+=1
                #if 0 then V eror
                if no_of_primary_org_units == 0:
                    self._errors["org_unit"] = self.error_class(['Please set one primary organisation unit for this person. No primary organisation selected.'])
        else:            
            self._errors["org_unit"] = self.error_class(['Please record atleast one primary organisation unit for this person'])  
            

        if (cleaned_data.get('nrc') != None):
            value = cleaned_data.get('nrc')
            if value:
                if not self.is_update_page:
                    if(RegPerson.objects.filter(national_id=value,is_void=False).count() > 0):
                        m_person = RegPerson.objects.get(national_id=value,is_void=False)          
                        self._errors["nrc"] = self.error_class(['This NRC is already linked to '+m_person.first_name+' '+m_person.other_names+' '+m_person.surname+'. You cannot register this NRC again.'])
                if self.is_update_page:
                    person_id = ''
                    if (cleaned_data.get('wfc_system_id') != None):
                        person_id = cleaned_data.get('wfc_system_id')
                    if(RegPerson.objects.filter(national_id=value,is_void=False).count() > 0):
                        m_person = RegPerson.objects.get(national_id=value,is_void=False)
                        if str(m_person.pk) == str(person_id):
                            #we do nothing really. just nothing
                            print 'they are equal so its fine'
                        else:                             
                            self._errors["nrc"] = self.error_class(['This NRC is already linked to %s %s %s. You cannot register this NRC again.' % (m_person.first_name, m_person.other_names, m_person.surname)])
            
                              
        if not self.is_update_page:      
            direct_services = ''
            nrc = ''
       
            
            if cleaned_data.get('direct_services') != None:
                direct_services = cleaned_data.get('direct_services')
            #print cleaned_data.get('nrc'),'self.cleaned_data.get(\'nrc\')'
            if cleaned_data.get('nrc') != None:
                nrc = cleaned_data.get('nrc')
            if direct_services == '2' and nrc == '': 
                if cleaned_data.get('nrc') is None:
                    pass
                else:
                    self._errors["nrc"] = self.error_class(['Please ensure that you enter an NRC for this person.'])
        '''
        workforce_type = ''       
        user_type_change_date = ''
        #print cleaned_data.get('workforce_type'),'cleaned_data.get(\'workforce_type\')'
        if cleaned_data.get('workforce_type') != None:
            workforce_type = cleaned_data.get('workforce_type')
            if cleaned_data.get('user_type_change_date') != None:
                user_type_change_date = cleaned_data.get('user_type_change_date')
            if workforce_type:
                if(RegPersonsTypes.objects.filter(person_type_id = workforce_type, date_ended = None, ic_void = False)).count() > 0:
                    m_reg_person_type = RegPersonsTypes.objects.get(person_type_id = workforce_type, date_ended = None, ic_void = False)
                    if user_type_change_date:
                        if m_reperson_type.date_began < user_type_change_date:
                            self._errors["__all__"] = self.error_class([u"Save not successful - please make corrections below"])
           '''
        if self._errors:
            self._errors["__all__"] = self.error_class([u"Save not successful - please make corrections below"])
          
        return self.cleaned_data

def compile_permissions(user=None,nrc=None,workforce_related_to_user=None,workforce_is_logged_in_user=None,roles_and_fields_to_show=None): 
    #nrc field
    nrc_special = ''
    nrc_general = ''
    #contact fields
    designated_phone_number = ''
    other_mobile_number = ''
    email_address = ''
    physical_address = ''
    #general fields
    workforce_type = ''
    man_number = ''
    ts_number = ''
    sign_number = ''
    steps_ovc_number = ''
    direct_services = ''
    org_unit = ''
    parent_org_change_date = ''
    primary_parent_org_unit = ''
    workforce_type_change_date = ''
    work_locations_change_date = ''
    date_of_death = ''
    
    if user:
        #Handle NRC special case
        if nrc:
            nrc_special = 'nrc'
            nrc_general = ''
            
        elif not nrc:
            nrc_special = ''
            nrc_general = 'nrc'
        #contact fields permissions
        if user.has_perm('auth.update wkf contact'):
            designated_phone_number = 'designated_phone_number'
            other_mobile_number = 'other_mobile_number'
            email_address = 'email_address'
            physical_address = 'physical_address'
            
        elif user.has_perm('auth.update wkf contact rel'):
            if workforce_related_to_user:
                designated_phone_number = 'designated_phone_number'
                other_mobile_number = 'other_mobile_number'
                email_address = 'email_address'
                physical_address = 'physical_address'
                
        elif user.has_perm('auth.update wkf contact own'):
            if workforce_is_logged_in_user:
                designated_phone_number = 'designated_phone_number'
                other_mobile_number = 'other_mobile_number'
                email_address = 'email_address'
                physical_address = 'physical_address'
        
        #general fields permissions
        if user.has_perm('auth.update wkf general'):
            workforce_type = 'workforce_type'
            man_number = 'man_number'
            ts_number = 'ts_number'
            sign_number = 'sign_number'
            steps_ovc_number = 'steps_ovc_number'
            direct_services = 'direct_services'
            org_unit = 'org_unit'
            primary_parent_org_unit = 'primary_parent_org_unit'
            parent_org_change_date = 'parent_org_change_date'
            workforce_type_change_date = 'workforce_type_change_date'
            work_locations_change_date = 'work_locations_change_date'
            date_of_death = 'date_of_death'
            
        elif user.has_perm('auth.update wkf general rel'):
            if workforce_related_to_user:
                workforce_type = 'workforce_type'
                man_number = 'man_number'
                ts_number = 'ts_number'
                sign_number = 'sign_number'
                steps_ovc_number = 'steps_ovc_number'
                direct_services = 'direct_services'
                org_unit = 'org_unit'
                primary_parent_org_unit = 'primary_parent_org_unit'
                parent_org_change_date = 'parent_org_change_date'
                workforce_type_change_date = 'workforce_type_change_date'
                work_locations_change_date = 'work_locations_change_date'
                date_of_death = 'date_of_death'
            
    roles_and_fields_to_show['Workforce Update'] ={ 'auth.update wkf special':['first_name', 'last_name', 'other_names', 'sex', 'date_of_birth',nrc_special,'edit_mode_hidden','workforce_id',direct_services],
	'auth.update wkf contact':[designated_phone_number, other_mobile_number, email_address, physical_address,'edit_mode_hidden','workforce_id',direct_services],
        'auth.update wkf contact own':[designated_phone_number, other_mobile_number, email_address, physical_address,'edit_mode_hidden','workforce_id',direct_services],
        'auth.update wkf contact rel':[designated_phone_number, other_mobile_number, email_address, physical_address,'edit_mode_hidden','workforce_id',direct_services],
        'auth.update wkf general':[workforce_type, man_number, ts_number, sign_number,steps_ovc_number,direct_services,org_unit,primary_parent_org_unit,workforce_type_change_date,parent_org_change_date,work_locations_change_date,nrc_general,date_of_death,'edit_mode_hidden','workforce_id'],
        'auth.update wkf general rel':[workforce_type, man_number, ts_number, sign_number,steps_ovc_number,direct_services,org_unit,primary_parent_org_unit,workforce_type_change_date,parent_org_change_date,work_locations_change_date,nrc_general,date_of_death,'edit_mode_hidden','workforce_id']
        }
    return roles_and_fields_to_show

def get_list_from_dic_or_querydict(obj, key):
    from django.http import QueryDict
    if not key in obj:
        return []
    if type(obj) == dict:
        return obj[key]
    if type(obj) == QueryDict:
        #print dir(obj)
        return obj.getlist(key)
def convert_to_int_array(valuetoconvert):
    if str(valuetoconvert).isdigit():
        return [int(valuetoconvert)]
    elif valuetoconvert:
        tmp = map(int, valuetoconvert)
        return tmp
    else:
        return None
