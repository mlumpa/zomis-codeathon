label_parent_unit = 'Primary parent organisational unit'
label_organisation_type = 'Organisational unit type'
label_about_organisation = 'About the organisational unit'
label_org_name = 'Name of organisational unit'
label_date_org_setup = 'Date organisational unit established'
label_date_closed = 'Date closed:'
label_legal_registration_type = 'Legal registration type'
label_legal_registratiom_number = 'Legal registration number'
label_land_phone_number = 'Landline phone number'
label_email_address = 'Email address'
label_postal_address  = 'Postal address'
label_physical_address  = 'Physical address'

error_msg_mobile_number_wrong_format = "Phone number must be entered in the format +26 followed by ten digits, eg '+260977123456'."
error_msg_org_name_name_exists = 'An organisational unit with this name already exists in the system.  Please check the registry to ensure this organisational unit is not already registered.  If this is a different organisational unit, please adjust the name, for example by putting the location in brackets after the name.'
error_msg_org_name_over_max_chars = 'Please use a name with less than 250 characters'
error_msg_parent_unit_is_required = 'Parent organisational unit is a required field. If this unit is a branch of an NGO, the parent unit is the NGO HQ.  If this unit is a district office of a Government Ministry, the parent unit is the Government Ministry.  If this unit is a stand-alone community based organization or committee, the parent unit is the local CWAC or Government-designated community-level supervising unit.  If this unit is a committee or stand-alone organization which is broader than one community but within one district, the parent unit is the district social welfare office.  If this unit is a national HQ of an NGO, the parent unit is MCDMCH HQ.'
error_msg_date_org_setup_in_future = 'Date organisation established cannot be in the future.'
error_msg_date_org_setup_date_after_date_closed = u"Date organisational unit established cannot be after the date the organisational unit was closed"
error_msg_date_closed_in_future = 'Date organisational unit was closed cannot be in the future.'
error_msg_date_closed_before_date_org_setup = u"Date closed cannot be before the date organisational unit was established."
error_msg_date_closed_is_empty = u"Please also select the date that the organisation was closed or ceased to be functional.  If the exact date is unknown, put an approximate date."
error_msg_legal_registration_number_required = u"Please fill in your organisational unit's NGO registration number"
error_msg_legal_registration_number_ngo_wrong_format = u"This NGO number is not in the correct format, please enter the MCDMCH-issued number in the format RNGO/101/NNNN/YY where NNNN is a serial number and YY is the year of registration"


error_msg_cwac_district = 'Please select one district'
error_msg_cwac_ward = 'Please select one ward'

error_msg_residential_children_district = 'Please select one district'
error_msg_residential_children_ward = 'Please select one ward'
error_msg_residential_children_community = 'Please select one community (CWAC or GDCLSU)'

error_msg_community_level_committee_district = 'Please select one district'
error_msg_district_level_committee_district = 'Please select one district'
error_msg_district_level_committee_ward = 'As this organisational unit type covers an entire district, individual wards should not be selected'
error_msg_district_level_committee_community = 'As this organisational unit type covers an entire district, individual communities should not be selected'

error_msg_ngo_private_multiple_district_district = 'Please choose one or more districts'
error_msg_ngo_private_multiple_district_ward = 'As this organisational unit type covers multiple districts, individual wards should not be selected'
error_msg_ngo_private_multiple_district_community = 'As this organisational unit type covers multiple districts, individual communities should not be selected'

error_msg_provincial_level_committee_district = 'As this orgainsational unit type is high-level, individual districts should not be selected'
error_msg_provincial_level_committee_ward = 'As this orgainsational unit type is high-level, individual wards should not be selected'
error_msg_provincial_level_committee_community = 'As this orgainsational unit type is high-level, individual communities should not be selected'

label_organisation_id = 'ZOMIS organisational unit ID:'

label_section_heading_organisation_type = 'Organisational unit type'
label_section_heading_about_organisation = 'About the organisational unit'
label_section_heading_services_location = 'Where located / providing services'
label_section_heading_contact_details = 'Organisational unit contact details'
label_section_heading_organisation_status = 'Organisational unit status'
label_section_heading_parent_unit = 'Primary parent organisational unit'



error_msg_overall_page_not_saved = u"Organisational unit not saved - please correct the errors in red below."