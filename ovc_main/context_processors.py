from django.conf import settings


def site_info(request):
    return {'is_capture_site': settings.IS_CAPTURE_SITE,
            'app_name': settings.APP_NAME}