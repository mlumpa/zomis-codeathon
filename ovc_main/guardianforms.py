   
'''
Created on Sep 29, 2014

@author: PKaumba
'''
from django import forms
from django.core.exceptions import ValidationError
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Fieldset, ButtonHolder, Submit, Button, HTML, MultiField, Div, Field
from crispy_forms.bootstrap import FormActions, AppendedText
from ovc_main.utils import fields_list_provider as db_fieldchoices, validators as validate_helper
from functools import partial
from ovc_main.models import RegPerson
from django.conf import settings
from ovc_main.utils.auth_access_control_util import del_form_field, filter_fields_for_display
#from pip._vendor.colorama.ansi import Style
DateInput = partial(forms.DateInput, {'class': 'datepicker'})

permission_type_fields = {  'rel_org':
                                {        
                                'auth.update ben contact by org':['physical_address','mobile_phone_number','email_address', 'edit_mode_hidden', 'pk_id'],
                                'auth.update ben general by org':['ben_district','ben_ward','community',
                                                                  'date_of_death','location_change_date','prev_loc_change_date','edit_mode_hidden', 'pk_id']
                                },
        
                            'other':
                                {
                                 'auth.update ben special':['first_name', 'last_name', 'other_name', 'sex', 'date_of_birth','dontskip','birth_cert_num','nrc','edit_mode_hidden', 'pk_id','workforce_member','date_paper_form_filled'],
                                 'auth.update ben contact by org':['physical_address','mobile_phone_number','email_address','dontskip', 'edit_mode_hidden', 'pk_id','workforce_member','date_paper_form_filled'],
                                'auth.update ben general by org':['ben_district','ben_ward','community',
                                                                  'date_of_death','location_change_date','prev_loc_change_date','dontskip', 'edit_mode_hidden', 'pk_id','workforce_member','date_paper_form_filled']
                                }
                          }

class GuardianRegistrationForm(forms.Form):
    is_update_page = False
    ben_id = ''
    beneficiary_id = forms.CharField(label='ZOMIS ID: ', required=False, initial = ben_id, widget=forms.HiddenInput())
    pk_id =  forms.CharField(label='Person_id: ', required=False, widget=forms.HiddenInput())
    
    #Identification information
    first_name = forms.CharField(label='First Name: ')
    last_name = forms.CharField(label='Surname: ')
    other_name = forms.CharField(required=False, label='Other Name(s): ')
    sex = forms.ChoiceField(label='Sex: ',choices=db_fieldchoices.get_sex_list())
    date_of_birth = forms.DateField(widget=DateInput('%d-%B-%Y'), input_formats=validate_helper.date_input_formats_tpl, validators=[validate_helper.validate_guardian_date_of_birth])
    nrc = forms.RegexField(regex=r'^\b\d{6}\/\b\d{2}\/\b[1-3]{1}$',error_message = ("NRC number must be entered in the format 987654/32/1"),label='NRC number: ', required=False)
    
    #Where the guardian lives
    ben_district = forms.ChoiceField(label='District: ', choices=db_fieldchoices.get_list_of_districts())
    ben_ward = forms.ChoiceField(label='Ward: ', choices=db_fieldchoices.get_list_of_wards())
    community = forms.ChoiceField(label='Community (CWAC or GDCLSU): ', choices=db_fieldchoices.get_list_of_cwacs())
    
    #Guardians contact details
    mobile_phone_number = forms.RegexField(required=False,label='Mobile phone number: ',regex=validate_helper.mobile_phone_number_regex, error_message = ("Phone number must be entered in the format: '+260977123456'. Up to 10 digits allowed."))
    email_address = forms.EmailField(required=False, label='Email address: ')
    physical_address = forms.CharField(required=False, widget=forms.Textarea(attrs={'rows': 4}), label='Physical address: ')
    
    #Paper Trail
    workforce_member = forms.CharField(required=False, label='Workforce member recorded on paper: ')
    date_paper_form_filled = forms.DateField(required=False, widget=DateInput(), input_formats=validate_helper.date_input_formats_tpl, validators=[validate_helper.validate_general_date])
    
    location_change_date = forms.DateField(widget=DateInput('%d %B, %Y'),required=False,label='', input_formats=validate_helper.date_input_formats_tpl, validators=[validate_helper.validate_general_date])
    date_of_death = forms.DateField(required=False, widget=DateInput('%d-%B-%Y'), input_formats=validate_helper.date_input_formats_tpl, validators=[validate_helper.validate_general_date])
    
    edit_mode_hidden = forms.CharField(required=False)
    
    res_id_hidden = forms.CharField(required=False)
    
    prev_loc_change_date = forms.DateField(required=False, widget=DateInput('%d %B, %Y'), input_formats=('%d %B %Y', '%d %B, %Y',))
        
    def __init__(self, user, *args, **kwargs):
        self.user = user        
        submitButtonText = 'Save'
        do_display_status = 'display:None'
        dont_display_status = ''
        actionurl = '/beneficiary/guardian/'
        header_text = """<div class="note note-info"><h4><strong>OVC Guardian Registration (ChildTrack form 3a)</strong>
        </h4><h5><p>This form is for registering guardians (ie primary caregivers) of orphans and vulnerable children. A guardian or primary caregiver is a 
        parent, a relative who is responsible for taking care of the basic needs of the child, or an adoptive or foster parent</h5></div>"""
        is_update_page = False
        if 'is_update_page' in kwargs:
            do_display_status = ''
            self.is_update_page = kwargs.pop('is_update_page')
            if self.is_update_page:
                header_text = """<div class="note note-info"><h4><strong>File Update Guardian Registration (ZOMIS form 3a)</strong></h4></div>"""
                actionurl = '/beneficiary/update_ben/'
                submitButtonText = 'Update'
                dont_display_status = 'display:None'
                
        #if self.is_update_page:
            #self.fields['date_paper_form_filled'] = forms.DateField(widget=DateInput('%d %B, %Y'), input_formats=validate_helper.date_input_formats_tpl, validators=[validate_helper.validate_general_date])
        #else:
            #self.fields['date_paper_form_filled'] = forms.DateField(required=False, widget=DateInput('%d %B, %Y'), input_formats=validate_helper.date_input_formats_tpl, validators=[validate_helper.validate_general_date])
        
        super(GuardianRegistrationForm, self).__init__(*args, **kwargs)
        
        self.fields['ben_district'] = forms.ChoiceField(label='District: ', choices=db_fieldchoices.get_list_of_districts())
        self.fields['ben_ward'] = forms.ChoiceField(label='Ward: ', choices=db_fieldchoices.get_list_of_wards())
        self.fields['community'] = forms.ChoiceField(label='Community (CWAC or GDCLSU): ', choices=db_fieldchoices.get_list_of_cwacs())
        
        #self.fields['location_change_date'].widget.attrs['readonly'] = True
        #self.fields['date_paper_form_filled'].widget.attrs['readonly'] = True
        #self.fields['date_of_death'].widget.attrs['readonly'] = True
        #self.fields['date_of_birth'].widget.attrs['readonly'] = True
        
        self.helper = FormHelper()
        
        self.helper.form_class = 'form-horizontal'
        self.helper.label_class = 'col-md-3'
        self.helper.attrs = {'padding-top':'3px'}
        self.helper.field_class = 'col-md-6'
        self.helper.form_action = actionurl
        self.helper.layout = Layout(
            MultiField(

                        header_text,
                        
                        Div(
                                Div(
                                    HTML('<p/>'),
                                    HTML("""<div id="div_id_beneficiary_id" class="form-group" style=\""""+do_display_status+"""\">
                                                <label for="id_benficiary_id" class="control-label col-md-3">ZOMIS beneficiary ID:</label>
                                                <div>
                                                  <p class="form-control-static col-md-6"><strong>{{beneficiary.beneficiary_id}}</strong></p>
                                                </div>
                                            </div>"""),
                                    'beneficiary_id',
                                    'pk_id',
                                    'nrc',
                                    css_class = 'panel-body pan',
                                    ),
                                css_class='panel panel-pink',
                            ),  
                        Div(
                                Div(HTML('<input type="radio" name="edit_mode" id="id_edit_mode" value="1" checked="checked"/> Update guardian details'
                                    ),
                                    css_class = 'panel-heading clearfix'
                                    ),
                                css_class='panel panel-green',
                                style = do_display_status,
                            ),                      
                        Div(
                                Div(HTML('Guardian identification information'),
                                    css_class = 'panel-heading'
                                    ),
                                Div(
                                    HTML('<p/>'),
                                    'first_name',
                                    'last_name',
                                    AppendedText('other_name','<span data-toggle="tooltip" title="Any other names which are either in official documents or are commonly used to refer to the person" data-placement="bottom"><i class="fa fa-info-circle"></i></span>'),
                                    'sex',
                                    AppendedText('date_of_birth','<span data-toggle="tooltip" title="If unknown, put estimate" data-placement="bottom"><i class="fa fa-info-circle"></i></span>'),
                                    css_class = 'panel-body pan',
                                    ),
                                css_class='panel panel-pink',
                            ),
                        Div(
                                Div(HTML('Where the guardian lives'),
                                    Div(
                                        Div(
                                            HTML('Location change date: '),
                                            Field(Div('location_change_date', style='display:inline'), css_class='form-inline'),
                                            css_class = 'form-inline',
                                            style = do_display_status,
                                            ),
                                        css_class= 'toolbars'
                                        ),
                                    css_class = 'panel-heading'
                                    ),
                                Div(
                                    HTML('<p/>'),
                                    Field(Div('edit_mode_hidden', style='visibility:hidden'), css_class='form-inline'),
                                    'ben_district',
                                    'ben_ward',
                                    'community',
                                    'physical_address',
                                    css_class = 'panel-body pan',
                                    ),
                                css_class='panel panel-grey',
                            ),
                        Div(
                                Div(HTML('Guardian contact details'),
                                    css_class = 'panel-heading'
                                    ),
                                
                                Div(
                                    HTML('<p/>'),
                                    Field(Div('prev_loc_change_date', style='visibility:hidden'), css_class='form-inline'),
                                    'mobile_phone_number',
                                    'email_address',
                                    Field(Div('res_id_hidden', style='visibility:hidden'), css_class='form-inline'),
                                    css_class = 'panel-body pan'
                                    ),
                                css_class='panel panel-pink',
                        
                            ),
                            Div(
                                Div(HTML('<input type="radio" name="edit_mode" id="id_edit_mode" value="2"/> Died'),
                                    css_class = 'panel-heading clearfix',
                                    style = do_display_status,
                                     ),
                                Div(
                                    HTML('<p/>'),
                                    Div('date_of_death', css_class='dead_control'),
                                    css_class = 'panel-body pan'
                                    ),
                                css_class='panel panel-green',
                                style = do_display_status,
                            ),
                            Div(
                                Div(HTML('<input type="radio" name="edit_mode" id="id_edit_mode" value="3"/> Guardian never existed (was a registration mistake or a duplicate)'),
                                    css_class = 'panel-heading clearfix',
                                    style = do_display_status,
                                    ),
                                css_class='panel panel-green',
                            ),
                        Div(
                                Div(HTML('Paper Trail'),
                                    Div(
                                        Div(
                                            HTML('<a href=\"/workforce/new_workforce/\" target=\"_blank\"><u>Register a workforce member</u></a>'),
                                            css_class = 'form-inline',
                                            style = dont_display_status,
                                            ),
                                        css_class= 'toolbars'
                                    ),
                                    css_class = 'panel-heading'
                                    ),
                                
                                Div(
                                    HTML('<p/>'),
                                    #Field(AppendedText('workforce_member','<span data-toggle="tooltip" title="If unknown, put estimate" data-placement="bottom"><i class="fa fa-info-circle"></i></span>'), style="display:inline", css_class="autocompletewfc form-inline", id="id_guardian "),
                                    Field('workforce_member', style="display:inline", css_class="autocompletewfc form-inline", id="id_guardian "),
                                    'date_paper_form_filled',
                                    css_class = 'panel-body pan'
                                    ),
                                Div(
                                    Div(
                                    HTML("""
                                    <table id="paperTrailTable"  class="table table-hover table-bordered">
                                        <thead>
                                        <tr>
                                            <th>Person recorded on paper</th>
                                            <th>Paper date</th>
                                            <th>Person recorded electronically</th>
                                            <th>Electronic date time</th>
                                            <th>Interface</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        {% if beneficiary.audit_trail  %}
                                        {% for audit in beneficiary.audit_trail %}
                                            <tr>
                                                <td id = "1">{{ audit.Person_recorded_on_paper }}</td>
                                                <td id = "2">{{ audit.Paper_date }}</td>
                                                <td id = "3">{{ audit.Person_recorded_electronically }}</td>
                                                <td id = "4">{{ audit.Electronic_date_time }}</td>
                                                <td id = "5">{{ audit.Interface }}</td>
                                            </tr>
                                        {% endfor %}
                                        {% endif %}
                                        </tbody>
                                    </table>
                                    
                                    <input id="ben_wfc_name_hidden" type="hidden">
                                    <input id="ben_wfc_id_hidden" type="hidden">
                                    <input id="ben_wfc_name_national_id" type="hidden">
                                    
                                    <script>
                                    function updateFinale(){
                                            //Todo: Implement some custom validations
                                            checkUpdateMode();
                                        }
                                        
                                    function checkUpdateMode(){
                                        var edit_mode = $('input:radio[name=edit_mode]:checked').val();
                                        $( "#id_edit_mode_hidden" ).val(edit_mode);                                            
                                    }
                                        
                                    </script>
                                    
                                  """),
                                    )
                                    ),
                                    css_class='panel panel-grey',
                          ),
                       Div(
                            HTML("<Button onclick=\"updateFinale()\" type=\"submit\" style=\"margin-right:4px\" class=\"mandatory btn btn-primary\"><i class=\"fa fa-floppy-o\"></i> "+submitButtonText+"</Button>"),
                            HTML("<a type=\"button\" href=\"/beneficiary/ben_search/\"class=\"mandatory btn btn-success\"><i class=\"fa fa-undo\"></i> Cancel</a><br><br>"),
                            style='margin-left:38%'
                        ),
            )
        )
        
        if self.is_update_page:
            layout = self.helper.layout
            searchitems = filter_fields_for_display(self.user, permission_type_fields)
            print searchitems, 'search items'
            if searchitems:
                del_form_field(layout, searchitems)
                self.helper.layout = layout
        
    def clean(self):
        if (self.cleaned_data.get('location_change_date') != None):
            entered_value = self.cleaned_data.get('location_change_date')
            if (self.cleaned_data.get('prev_loc_change_date') != None):
                date_linked_value = self.cleaned_data.get('prev_loc_change_date')
                if entered_value < date_linked_value:
                    raise ValidationError("Location change date cannot be less than the the date linked")
                
        if self.is_update_page:
                if (self.cleaned_data.get('date_of_birth') != None):
                    dob = self.cleaned_data.get('date_of_birth')
                    if (self.cleaned_data.get('date_of_death') != None):
                        dod = self.cleaned_data.get('date_of_death')
                        if dod:
                            if dod < dob:
                                self._errors["date_of_death"] = self.error_class([u"Date of death cannot be less than date of birth"])
        
        if (self.cleaned_data.get('nrc') != None or self.cleaned_data.get('first_name') != None or self.cleaned_data.get('last_name') != None):
            
            nrc_id = self.cleaned_data.get('nrc')
            f_name = self.cleaned_data.get('first_name')
            l_name = self.cleaned_data.get('last_name')
            
            if nrc_id or (f_name and l_name):
                if not self.is_update_page:
                                        
                    if(RegPerson.objects.filter(national_id=nrc_id,is_void=False,date_of_death = None).count() > 0):
                        person = RegPerson.objects.filter(national_id=nrc_id,is_void=False,date_of_death = None)
                        m_person = None
                        for m_per in person:
                            m_person = m_per
                        
                        if m_person.national_id:
                            msg = "["+m_person.national_id+"],["+m_person.beneficiary_id+"], ["+m_person.first_name+", "+m_person.surname+", "+m_person.other_names+"] is already in this registry."
                        else:
                            msg = "This NRC is already in the system. You cannot register this NRC again."
                        if m_person.national_id:    
                            raise ValidationError(msg)
                    
                    if(RegPerson.objects.filter(first_name=f_name,surname=l_name,is_void=False,date_of_death = None).count() > 0):
                        person = RegPerson.objects.filter(first_name=f_name,surname=l_name,is_void=False,date_of_death = None)
                        m_person = None
                        for m_per in person:
                            m_person = m_per
                        
                        if m_person:
                            msg = "["+m_person.national_id+"],["+m_person.beneficiary_id+"], ["+m_person.first_name+", "+m_person.surname+", "+m_person.other_names+"] is already in this registry."# Do you want to save?"
                        else:
                            msg = "This person is already in the system."
                        print ValidationError
                        raise ValidationError(msg)
                            
        return self.cleaned_data
    '''            
        if (self.cleaned_data.get('nrc') != None):
            value = self.cleaned_data.get('nrc')
            if value:
                if not self.is_update_page:
                    if(RegPerson.objects.filter(national_id=value,is_void=False,date_of_death = None).count() > 0):
                        person = RegPerson.objects.filter(national_id=value,is_void=False,date_of_death = None)
                        m_person = None
                        for m_per in person:
                            m_person = m_per
                        
                        if m_person:
                            msg = "This NRC is already linked to "+m_person.first_name+" "+m_person.other_names+" "+m_person.surname+". You cannot register this NRC again."
                        else:
                            msg = "This NRC is already in the system. You cannot register this NRC again." 
                            
                        raise ValidationError(msg) 
     
        return self.cleaned_data
        '''
