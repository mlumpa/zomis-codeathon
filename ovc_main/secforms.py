from django import forms
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Fieldset, ButtonHolder, Submit, Button, HTML, MultiField, Div, Field
from crispy_forms.bootstrap import FormActions, AppendedText
from ovc_main.models import SetupList, SetupGeorgraphy
from ovc_main.utils.fields_list_provider import get_org_list
from functools import partial
from ovc_main.utils.geo_location import get_communities_in_ward
DateInput = partial(forms.DateInput, {'class': 'datepicker'})
import datetime


class GeographyRoleForm(forms.Form):
    pass
class OrganisationRoleForm(forms.Form):
    pass