from ovc_main.models import RegOrgUnitsAuditTrail, \
    RegPerson, RegOrgUnit, RegPersonsTypes, RegPersonsAuditTrail, \
    RegPersonsGuardians, RegPersonsGeo, SetupGeorgraphy, CoreAdverseConditions
from ovc_main.utils import fields_list_provider
from django.db.models import Count
from ovc_main.utils import geo_location
import datetime
import time
import calendar
from ovc_main.utils import lookup_field_dictionary as fdict
from ovc_main.utils.fields_list_provider import calculate_age

def orgs_update_counts(start_date=None, end_date=None, monthsoffset=None):
    
    params = {}
    if monthsoffset:
        start_date, end_date = get_date_range(start_date, monthoffset=monthsoffset)
    else:
        start_date, end_date = get_date_range(start_date, end_date)

    if isinstance(end_date, datetime.date):
        end_date = datetime.datetime(end_date.year, end_date.month, end_date.day, 23, 59, 59)
    params['transaction_type_id'] = 'UPDU'
    orgs_edited_in_period = RegOrgUnitsAuditTrail.objects.filter(timestamp_modified__gte=start_date, 
                                                timestamp_modified__lte=end_date,transaction_type_id='UPDU').values_list('org_unit_id', 'timestamp_modified')
                                             
    return orgs_edited_in_period
                      
def new_orgs_registered(start_date=None, end_date=None, monthsoffset=None):
    
    if monthsoffset:
        start_date, end_date = get_date_range(start_date, monthoffset=monthsoffset)
    else:
        start_date, end_date = get_date_range(start_date, end_date)
    
    if isinstance(end_date, datetime.date):
        end_date = datetime.datetime(end_date.year, end_date.month, end_date.day, 23, 59, 59)
    orgs_edited_in_period = RegOrgUnitsAuditTrail.objects.filter(timestamp_modified__gte=start_date, 
                                                timestamp_modified__lte=end_date, 
                                                transaction_type_id='REGU').values_list('org_unit_id', 'timestamp_modified')
    return orgs_edited_in_period

def beneficiary_registered_list(start_date=None, end_date=None, monthsoffset=None):
    
    if monthsoffset:
        start_date, end_date = get_date_range(start_date, monthoffset=monthsoffset)
    else:
        start_date, end_date = get_date_range(start_date, end_date)
    
    if isinstance(end_date, datetime.date):
        end_date = datetime.datetime(end_date.year, end_date.month, end_date.day, 23, 59, 59)
    orgs_edited_in_period = RegPersonsAuditTrail.objects.filter(timestamp_modified__gte=start_date, 
                                                timestamp_modified__lte=end_date, 
                                                transaction_type_id='REGB').values_list('person_id', 'timestamp_modified')
    return orgs_edited_in_period

def beneficiary_updated_list(start_date=None, end_date=None, monthsoffset=None):
    
    if monthsoffset:
        start_date, end_date = get_date_range(start_date, monthoffset=monthsoffset)
    else:
        start_date, end_date = get_date_range(start_date, end_date)
    
    if isinstance(end_date, datetime.date):
        end_date = datetime.datetime(end_date.year, end_date.month, end_date.day, 23, 59, 59)
    orgs_edited_in_period = RegPersonsAuditTrail.objects.filter(timestamp_modified__gte=start_date, 
                                                timestamp_modified__lte=end_date, 
                                                transaction_type_id='UPDB').values_list('person_id', 'timestamp_modified')
    return orgs_edited_in_period


def new_workforce_user(start_date=None, end_date=None, monthsoffset=None):
    
    if monthsoffset:
        start_date, end_date = get_date_range(start_date, monthoffset=monthsoffset)
    else:
        start_date, end_date = get_date_range(start_date, end_date)
    
    if isinstance(end_date, datetime.date):
        end_date = datetime.datetime(end_date.year, end_date.month, end_date.day, 23, 59, 59)
        
    new_reg_in_period = RegPersonsAuditTrail.objects.filter(timestamp_modified__gte=start_date, 
                                                timestamp_modified__lte=end_date, 
                                                transaction_type_id__in=['REGW','REGS']).values_list('person_id', 'timestamp_modified')
                                            
    return new_reg_in_period

def updated_workforce_user(start_date=None, end_date=None, monthsoffset=None):
    
    if monthsoffset:
        start_date, end_date = get_date_range(start_date, monthoffset=monthsoffset)
    else:
        start_date, end_date = get_date_range(start_date, end_date)
    
    if isinstance(end_date, datetime.date):
        end_date = datetime.datetime(end_date.year, end_date.month, end_date.day, 23, 59, 59)
        
    orgs_edited_in_period = RegPersonsAuditTrail.objects.filter(timestamp_modified__gte=start_date, 
                                                timestamp_modified__lte=end_date, 
                                                transaction_type_id__in=['UPDW','UPDS']).values_list('person_id', 'timestamp_modified')
                                            
    return orgs_edited_in_period

def workforce_user_count_by_type():
    person_types = {
                    fdict.workforce_type_gov_id:'Government Staff', 
                    fdict.workforce_type_ngo_id: 'NGO/Private Staff',
                    fdict.workforce_type_vol_id: 'Volunteer'
                    }
    
    person_types_counts = {
                           person_types[fdict.workforce_type_gov_id]: 0,
                           person_types[fdict.workforce_type_ngo_id]: 0,
                           person_types[fdict.workforce_type_vol_id]: 0
                           }
    
    for persontype in RegPersonsTypes.objects.filter(is_void=False, date_ended=None, person_type_id__in=person_types.keys()):
        person_types_counts[person_types[persontype.person_type_id]] += 1
    
    return person_types_counts
 
def workforce_user_counts():
    person_types = {}
    person_types['User'] = fdict.register_user
    person_types['Workforce'] = fdict.register_workforce_member
    person_type_counts = {fdict.register_user:0, fdict.register_workforce_member:0}

    for person in RegPersonsAuditTrail.objects.filter(transaction_type_id__in=person_types.values()):
        if person.transaction_type_id in person_type_counts:
            person_type_counts[person.transaction_type_id] += 1

    return person_type_counts[fdict.register_workforce_member], person_type_counts[fdict.register_user]

def orgs_count():
    orgs_count = 0
    for org in RegOrgUnit.objects.filter(is_void=False):
        if not org.is_active:
            continue
        orgs_count = orgs_count + 1
    return orgs_count

def guardian_count():
    guardian_count = 0
    for guardian in RegPersonsTypes.objects.filter(person_type_id='TBGR', is_void=False, date_ended=None):
        if guardian.person.date_of_death:
            continue
        guardian_count +=1
    return guardian_count

def children_count():
    children_count = 0
    for child in RegPersonsTypes.objects.filter(person_type_id='TBVC', is_void=False, date_ended=None):
        age = calculate_age(child.person.date_of_birth)
        if age < 18:
            children_count += 1
    return children_count

def organisation_counts_by_type():  
    unit_type_count = {}

    for org in RegOrgUnit.objects.filter(is_void=False):
        if not org.is_active:
            continue
        if org.unit_type_id in unit_type_count:
            unit_type_count[org.unit_type_id] += 1
        else:
            unit_type_count[org.unit_type_id] = 1
    
    from ovc_main.models import SetupList
    
    org_types_detail = {item.item_id:item.item_description for item in SetupList.objects.filter(item_id__in=unit_type_count.keys())}
    print org_types_detail
    print unit_type_count
    to_return = {org_types_detail[org_type_id]:org_type_count for org_type_id, org_type_count in unit_type_count.items()}
    return to_return
def dashboard_children_section():
    to_return = {}
    
    age_range_birth_reg = {
         0:{'SMAL':0, 'SFEM':0},
         5:{'SMAL':0, 'SFEM':0},
         10:{'SMAL':0, 'SFEM':0},
         15:{'SMAL':0, 'SFEM':0},
         }
    age_range_no_birth_reg = {
         0:{'SMAL':0, 'SFEM':0},
         5:{'SMAL':0, 'SFEM':0},
         10:{'SMAL':0, 'SFEM':0},
         15:{'SMAL':0, 'SFEM':0},
         }
    
    children_ids = []
    for child in RegPersonsTypes.objects.filter(person_type_id='TBVC', is_void=False, date_ended=None):
        dob = child.person.date_of_birth
        sex = child.person.sex_id
        birthreg = child.person.birth_reg_id
        
        if child.person.date_of_death:
            continue
        
        if not dob:
            continue
        age = calcuate_age(dob)
        if age < 18:
            children_ids.append(child.person.pk)
            
        if birthreg:
            if age < 5:
                age_range_birth_reg[0][sex] +=1
            elif age < 10:
                age_range_birth_reg[5][sex] +=1
            elif age < 15:
                age_range_birth_reg[10][sex] +=1
            elif age < 18:
                age_range_birth_reg[15][sex] +=1
        else:
            if age < 5:
                age_range_no_birth_reg[0][sex] +=1
            elif age < 10:
                age_range_no_birth_reg[5][sex] +=1
            elif age < 15:
                age_range_no_birth_reg[10][sex] +=1
            elif age < 18:
                age_range_no_birth_reg[15][sex] +=1
    
    values_birthreg = []
    values_no_birthreg = []
    order_of_labels = []
    labels = {0:{'SMAL': 'Male 0-4',
                 'SFEM': 'Female 0-4'
                 },
              5:{'SMAL': 'Male 5-9',
                 'SFEM': 'Female 5-9'
                 },
              10:{'SMAL': 'Male 10-14',
                 'SFEM': 'Female 10-14'
                 },
              15:{'SMAL': 'Male 15-17',
                 'SFEM': 'Female 15-17'
                 }
              
               }
    for agerange in age_range_birth_reg:
        values_birthreg.append(age_range_birth_reg[agerange]['SMAL'])
        order_of_labels.append(labels[agerange]['SMAL'])
        values_birthreg.append(age_range_birth_reg[agerange]['SFEM'])
        order_of_labels.append(labels[agerange]['SFEM'])
        
        values_no_birthreg.append(age_range_no_birth_reg[agerange]['SMAL'])
        values_no_birthreg.append(age_range_no_birth_reg[agerange]['SFEM'])
    
    
     
    children_area_ids = [(persongeo.person_id, persongeo.area_id) for persongeo in RegPersonsGeo.objects.filter(person_id__in=children_ids, is_void=False, date_delinked=None)]
    children_areas = {}
    for childid, area_id in children_area_ids:
        child_areas = geo_location.get_geo_ancestors_level_info(area_id)
        for geoid, geo_name, geo_level in child_areas:
            if geo_level != 'province' or childid in children_areas:
                continue
            children_areas[childid] = geo_name
    
    provinces_dict = {}
    for province in children_areas.values():
        if province in provinces_dict:
            provinces_dict[province] += 1
        else:
            provinces_dict[province] = 1
            
    provinces_to_return = []
    for prov, count in provinces_dict.items():
        provinces_to_return.append([prov,count])
        
    top_adverse_conds, sum_all_conds = top_adverse_conditions() 
    
    conds_for_progress_bar = format_for_progress_bar(top_adverse_conds, sum_all_conds)
    
    return {'children_age_range':
                {'categories':order_of_labels, 'values_birth_reg':values_birthreg, 'values_no_birth_reg':values_no_birthreg},
            'registered_by_province': provinces_to_return,
            'top_adverse_conds': conds_for_progress_bar
        }

def adverse_cond_aggregate():   
    adverse_conds_counts = CoreAdverseConditions.objects.filter(is_void=0).values_list('adverse_condition_id').annotate(num_conds=Count('adverse_condition_id')).order_by()
    return adverse_conds_counts

def top_adverse_conditions(num_top_conds=10):
    top_adverse_conds = []
    top_adverse_conds_counts = []
    total_adverse_conds = 0
    
    adverse_conds_counts = adverse_cond_aggregate()
    full_list_of_adverse_conditions = fields_list_provider.adverse_conditions()
    top_adverse_cond_dict = {}
    for adverse_cond_id, count in adverse_conds_counts:
        total_adverse_conds += count
        if len(top_adverse_conds) <= num_top_conds:
            top_adverse_cond_dict[full_list_of_adverse_conditions[adverse_cond_id]] = count
        
    return top_adverse_cond_dict, total_adverse_conds
            
def calcuate_age(dob):
    days_in_year = 365.2425    
    age = int((datetime.date.today() - dob).days / days_in_year)
    return age
    
    
def dashboard_reg_summary():
    to_return = {}
    summary = {'orgs':0, 'workforce':0, 'user':0, 'guardian':0, 'children':0}
    summary['orgs'] = orgs_count()
    summary['workforce'], summary['user'] = workforce_user_counts()
    summary['guardian'] = guardian_count()
    summary['children'] = children_count()
    
    workforce_user_counts_by_type = format_for_radar(workforce_user_count_by_type())
    
    organisation_type_count = format_for_progress_bar(organisation_counts_by_type(), orgs_count())
    
    registries = org_new_update_counts_past_months()
    
    to_return = {'summary':summary, 'workforce_type':workforce_user_counts_by_type, 'orgs_type':organisation_type_count, 'registries': registries}
    
    return to_return


def org_new_update_counts_past_months(num_past_months=3):
    num_past_months *= (-1)
    
    dateranges = []
    
    end_date = datetime.datetime.now().date()
    start_date = add_months(end_date, num_past_months)
    
    dateranges.append(start_date)
    dateranges.append(end_date)
    
    if abs(num_past_months) > 1:
        for monthoffset in range(1, abs(num_past_months)):
            prev_month_date = add_months(end_date, (-monthoffset))
            dateranges.append(prev_month_date)
    
    new_orgs = new_orgs_registered(start_date, end_date)
    orgs_counts_by_month = group_by_date(new_orgs, dateranges)
    del orgs_counts_by_month[end_date]
    
    update_orgs = orgs_update_counts(start_date, end_date)
    orgs_update_counts_by_month = group_by_date(update_orgs, dateranges)
    del orgs_update_counts_by_month[end_date]
    
    new_workforce_user_list = new_workforce_user(start_date, end_date)
    new_workforce_user_counts_by_month = group_by_date(new_workforce_user_list, dateranges)
    del new_workforce_user_counts_by_month[end_date]
    
    update_workforce_user_list = updated_workforce_user(start_date, end_date)
    update_workforce_user_counts_by_month = group_by_date(update_workforce_user_list, dateranges)
    del update_workforce_user_counts_by_month[end_date]
    
    new_ben_list = beneficiary_registered_list(start_date, end_date)
    new_ben_list_count_by_month = group_by_date(new_ben_list, dateranges)
    del new_ben_list_count_by_month[end_date]
    
    updated_ben_list = beneficiary_updated_list(start_date, end_date)
    updated_ben_list_count_by_month = group_by_date(updated_ben_list, dateranges)
    del updated_ben_list_count_by_month[end_date]
    
    dict_to_return = {}
    for date_count in orgs_counts_by_month:
        dict_to_return[date_count.strftime('%d-%b-%y')] = {
                                      'org_new': orgs_counts_by_month[date_count], 
                                      'org_updated':orgs_update_counts_by_month[date_count],
                                      'workforce_new': new_workforce_user_counts_by_month[date_count], 
                                      'workforce_updated':update_workforce_user_counts_by_month[date_count],
                                      'beneficiary_new': new_ben_list_count_by_month[date_count], 
                                      'beneficiary_updated':updated_ben_list_count_by_month[date_count],
                                     }
    return dict_to_return

    
def group_by_date(item_date_tuple_list, date_range_list):
    
    date_range_list.sort()
    date_range_counts = {cur_date:0 for cur_date in date_range_list}
    prev_date = None
    for item, item_datetime in item_date_tuple_list:
        is_first = True
        for date_to_check in date_range_list:
            #the first time, we just want to retrieve the start date
            if is_first:
                prev_date = date_to_check
                is_first = False
                continue
            #we check that the date_time we are looking at is less than the date we previously checked
            #and its next date. The values in the date_range_list should always be sorted
            if item_datetime.date() >= prev_date and item_datetime.date() < date_to_check:
                date_range_counts[prev_date] += 1
            prev_date = date_to_check

        if date_to_check and item_datetime and item_datetime.date() == date_to_check:
            date_range_counts[date_range_list[-2:-1][0]] += 1
    return date_range_counts

def format_for_radar(report_dict):
    return {
            'labels':report_dict.keys(), 
            'values':report_dict.values()}  


def format_for_progress_bar(report_dict, total_for_dict=None, max_progress=100): 
    import operator 
    to_return = {item_key:[item_value, total_for_dict] for item_key, item_value in report_dict.items()}
    sorted_x = sorted(to_return.items(), key=operator.itemgetter(1))
    return to_return

def get_date_range(start_date=None, end_date=None, monthoffset=None):
    if not start_date:
        start_date = datetime.datetime.min
    if monthoffset:
        end_date = add_months(start_date, monthoffset)
    elif not end_date:
        end_date = datetime.datetime.max
        
    return start_date, end_date

#add_months: http://stackoverflow.com/a/4131114
def add_months(source_date, num_months):
    month = source_date.month - 1 + num_months
    year = source_date.year + month/12
    month = month % 12 + 1
    day = min(source_date.day, calendar.monthrange(year, month)[1])
    return datetime.date(year, month, day)

