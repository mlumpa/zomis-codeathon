'''
Created on Sep 2, 2014

@author: MLumpa
'''
from django.conf.urls import patterns, url

from ovc_main import views

urlpatterns = patterns('',
    url(r'^$', views.index, name='index'),
    url(r'^new/', views.new_organisation, name='new-organisation'),
    url(r'^wards/', views.listofwards, name='listofwards'),
    url(r'^orglists/', views.org_search_results, name='org_search_results'),
    url(r'^view_org/', views.organisation_view, name='organisation_view'),
    url(r'^update_org/', views.update_organisation, name='organisation_update'),
    url(r'^wfcsearch/', views.workforce_index, name='wfc_search_index'),
    url(r'^wfclists/', views.wfc_search_results, name='wfc_search_index'),
    url(r'^new_workforce/', views.new_workforce, name='workforce'),
    url(r'^orgautocomp/', views.org_autocompletion_list, name='orgautocomplete'),
    url(r'^view_wfc/', views.workforce_view, name='workforce_view'),
    url(r'^update_wfc/', views.workforce_update, name='workforce_update'),
    url(r'^communities/', views.list_of_cwacs_in_ward, name='list_of_cwacs_in_ward'),
    url(r'^ben_search/', views.search_beneficiary, name='ben_search'),
    url(r'^guardian/', views.new_guardian_reg, name='guardian'),
    url(r'^child/', views.new_child_reg, name='child'),
    url(r'^ben_autocomplete/', views.ben_autocompletion_list, name='benautocomplete'),
    url(r'^ben_wfc_autocomplete/', views.ben_wfc_autocompletion_list, name='benwfcautocomplete'),
    url(r'^ben_res_autocomplete/', views.ben_res_autocompletion_list, name='benresautocomplete'),
    url(r'^benlists/', views.ben_search_results, name='ben_search_results'),
    url(r'^view_ben/', views.beneficiary_view, name='beneficiary_view'),
    url(r'^update_ben/', views.update_beneficiary, name='beneficiary_update'),
    url(r'^guardian_contact/', views.get_guardian_contacts, name='guard_contact'),
    url(r'^capture_admin/', views.capture_admin, name='capture_administration'),
    url(r'^begin_upload/', views.start_upload, name='start_upload'),
    url(r'^begin_download/', views.start_download, name='start_download'),
    url(r'^upload/', views.process_upload, name='api_capture_upload'),
    url(r'^download/', views.download, name='download_sections'),
    #url(r'^persons/', views.get_persons_list, name='api_persons_list'),
)
