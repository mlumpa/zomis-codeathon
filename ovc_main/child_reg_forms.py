   
'''
Created on Sep 29, 2014

@author: PKaumba
'''
from django import forms
from django.core.exceptions import ValidationError
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Fieldset, ButtonHolder, Submit, Button, HTML, MultiField, Div, Field
from crispy_forms.bootstrap import FormActions, AppendedText
from ovc_main.utils import fields_list_provider as db_fieldchoices, validators as validate_helper
from ovc_main.utils.general import convert_to_int_array, get_list_from_dic_or_querydict
from functools import partial
from ovc_main.models import RegPerson
from django.conf import settings
from ovc_main.utils.auth_access_control_util import del_form_field, filter_fields_for_display

#from pip._vendor.colorama.ansi import Style
DateInput = partial(forms.DateInput, {'class': 'datepicker'})

permission_type_fields = {  'rel_org':
                                {        
                                'auth.update ben contact by org':['physical_address','mobile_phone_number','email_address', 'edit_mode_hidden', 'pk_id'],
                                'auth.update ben general by org':['STEPS_OVC_number','residential_institution','ben_district','ben_ward','community',
                                                                  'date_of_death','location_change_date','prev_loc_change_date','guardian_change_date',
                                                                  'prev_guard_change_date','guardian','notes_relationship','guardians_hidden','edit_mode_hidden',
                                                                  'pk_id']
                                },
        
                            'other':
                                {
                                 'auth.update ben special':['first_name', 'last_name', 'other_name', 'sex', 'date_of_birth','birth_cert_num','nrc','edit_mode_hidden', 
                                                            'pk_id','workforce_member','date_paper_form_filled'],
                                 'auth.update ben contact by org':['physical_address','mobile_phone_number','email_address', 'edit_mode_hidden', 'pk_id','workforce_member','date_paper_form_filled'],
                                'auth.update ben general by org':['STEPS_OVC_number','residential_institution','ben_district','ben_ward','community','date_of_death',
                                                                  'location_change_date','prev_loc_change_date','no_adult_guardians','guardian_change_date',
                                                                  'prev_guard_change_date','guardian','notes_relationship','guardians_hidden','dontskip', 'edit_mode_hidden', 
                                                                  'pk_id','workforce_member','date_paper_form_filled']
                                }
                          }


class ChildRegistrationForm(forms.Form):
    is_update_page = False
    #is_update_p = forms.CharField(required=False)
    
    #Child Identification information
    first_name = forms.CharField(label='First Name: ')
    last_name = forms.CharField(label='Last Name: ')
    other_name = forms.CharField(required=False, label='Other Names: ')
    sex = forms.ChoiceField(label='Sex: ',choices=db_fieldchoices.get_sex_list())
    #steps_ovc_number = forms.CharField(required=False)
    date_of_birth = forms.DateField(required=True, widget=DateInput('%d %B, %Y'), input_formats=('%d %B %Y', '%d %B, %Y',), validators=[validate_helper.validate_child_date_of_birth])
    birth_cert_num = forms.RegexField(regex=r'^\b\w{3}\/\b\d{6}\/\d{4}$',required=False, label='Birth certificate registration number: ',error_message = ("Birth certificate number must be entered in the format PRO/123456/YYYY"))
    nrc = forms.RegexField(regex=r'^\b\d{6}\/\b\d{2}\/\b[1-3]{1}$',error_message = ("NRC number must be entered in the format 987654/32/1"),label='NRC number: ', required=False)
    pk_id = forms.CharField(label='pk_id: ', required=False, widget=forms.HiddenInput())
    STEPS_OVC_number = forms.CharField(required=False)
    
    #Where the child lives
    residential_institution = forms.CharField(required=False, label='Residntial instituition: ')
    ben_district = forms.ChoiceField(label='District: ', choices=db_fieldchoices.get_list_of_districts())
    ben_ward = forms.ChoiceField(label='Ward: ', choices=db_fieldchoices.get_list_of_wards())
    community = forms.ChoiceField(label='Community (CWAC or GDCLSU): ', choices=db_fieldchoices.get_list_of_cwacs())
    physical_address = forms.CharField(required=False, widget=forms.Textarea(attrs={'rows': 4}), label='Physical address / Description of where living: ')
    
    #Child's guardians/primary caregivers
    no_adult_guardians = forms.BooleanField(required = False)
    guardian = forms.CharField(required=False, label='Guardian: ')
    notes_relationship = forms.CharField(required=False, label='Notes on relationship with child: ')
    guardians_hidden = forms.CharField(required=False)
    
    #Adverse conditions (Click as many as apply, either now or in the past)
    #adverse_cond = forms.MultipleChoiceField(widget=forms.CheckboxSelectMultiple, choices=db_fieldchoices.get_list_adverse_cond(), label='.')
    
    #Child's contact details
    mobile_phone_number = forms.RegexField(required=False,label='Mobile phone number: ',regex=validate_helper.mobile_phone_number_regex, error_message = ("Phone number must be entered in the format: '+260977123456'. Up to 10 digits allowed."))
    email_address = forms.EmailField(required=False, label='Email address: ')
    res_id_hidden = forms.CharField(required=False)
    
    #Died
    date_of_death = forms.DateField(required=False, widget=DateInput(), input_formats=validate_helper.date_input_formats_tpl, validators=[validate_helper.validate_general_date]) #format='%d-%B-%Y'
    
    #Paper Trail
    workforce_member = forms.CharField(required=False, label='Workforce member recorded on paper: ')
    date_paper_form_filled = forms.DateField(required=False, widget=DateInput(), input_formats=validate_helper.date_input_formats_tpl, validators=[validate_helper.validate_general_date])
    
    guardian_change_date = forms.DateField(widget=DateInput('%d %B, %Y'),required=False,label='', input_formats=validate_helper.date_input_formats_tpl, validators=[validate_helper.validate_general_date])
    location_change_date = forms.DateField(widget=DateInput('%d %B, %Y'),required=False,label='', input_formats=validate_helper.date_input_formats_tpl, validators=[validate_helper.validate_general_date])
    
    
    prev_guard_change_date = forms.DateField(required=False, widget=DateInput('%d %B, %Y'), input_formats=('%d %B %Y', '%d %B, %Y',))
    prev_loc_change_date = forms.DateField(required=False, widget=DateInput('%d %B, %Y'), input_formats=('%d %B %Y', '%d %B, %Y',))
    adverse_cond =  forms.MultipleChoiceField(required = False, choices=db_fieldchoices.get_list_adverse_cond(), label='.')
    #Update Dates
    #location_change_date = forms.DateField(widget=DateInput(),required=False,label='')
    #guardian_change_date = forms.DateField(widget=DateInput(),required=False,label='Guardian change date: ')
    
    #edit_mode = forms.ChoiceField(required=False)
    
    
    edit_mode_hidden = forms.CharField(label='',required=False)
    
    def __init__(self, user,*args, **kwargs):
        self.user = user
        is_update_page = False
        do_display_status = 'display:None'
        hide_status = 'text'
        dont_display_status = ''
        submitButtonText = 'Save'
        actionurl = '/beneficiary/child/'
        display_date_closed_control = 'visible'
        header_text = """<div class="note note-info"><h4><strong>OVC Registration (ChildTrack form 3b)</strong>
                        </h4><h5><p>This form is for registering orphans and vulnerable children</h5></div>"""
        
        districtid = None
        wardid = None
        if 'is_update_page' in kwargs:
            self.is_update_page = kwargs.pop('is_update_page')
            if self.is_update_page:
                do_display_status = ''
                dont_display_status = 'display:None'
                header_text = """<div class="note note-info"><h4><strong>OVC Registration ("""+settings.APP_NAME+""" form 3b)</strong></div>"""
                actionurl = '/beneficiary/update_ben/'
                submitButtonText = 'Update'
                hide_status = 'visibility:hidden'
                
                if args and 'ben_district' in args[0]:
                    districtid = args[0]['ben_district']
                
                wardsoptionlist = ''
                if args and 'ben_ward' in args[0]:
                    wardid = get_list_from_dic_or_querydict(args[0], 'ben_ward')
                    
                    if args and 'community' in args[0]:
                        communities = get_list_from_dic_or_querydict(args[0], 'community')
                
        super(ChildRegistrationForm, self).__init__(*args, **kwargs)
        
        if self.is_update_page:
            self.fields['adverse_cond'] =  forms.MultipleChoiceField(required = False, widget=forms.HiddenInput(), choices=db_fieldchoices.get_list_adverse_cond(), label='.')
            self.fields['date_paper_form_filled'] = forms.DateField(widget=DateInput('%d %B, %Y'), input_formats=validate_helper.date_input_formats_tpl, validators=[validate_helper.validate_general_date])
            self.fields['guardian_change_date'] = forms.DateField(required=False, widget=DateInput('%d %B, %Y'),label='', input_formats=validate_helper.date_input_formats_tpl, validators=[validate_helper.validate_general_date])
            self.fields['ben_ward'] = forms.ChoiceField(label='Ward: ', choices=db_fieldchoices.get_list_of_wards(districtid))
            wards = db_fieldchoices.get_list_of_wards(districtid)
            self.fields['community'] = forms.ChoiceField(label='Community (CWAC or GDCLSU): ', choices=db_fieldchoices.get_list_of_cwacs(wards))
            self.fields['location_change_date'] = forms.DateField(widget=DateInput('%d %B, %Y'),required=True,label='', input_formats=validate_helper.date_input_formats_tpl, validators=[validate_helper.validate_general_date])
            
        else:
            self.fields['adverse_cond'] =  forms.MultipleChoiceField(widget= forms.CheckboxSelectMultiple, choices=db_fieldchoices.get_list_adverse_cond(), label='')
            self.fields['date_paper_form_filled'] = forms.DateField(required=False, widget=DateInput('%d %B, %Y'), input_formats=validate_helper.date_input_formats_tpl, validators=[validate_helper.validate_general_date])
            self.fields['guardian_change_date'] = forms.DateField(required=False, widget=DateInput('%d %B, %Y'),label='', input_formats=validate_helper.date_input_formats_tpl, validators=[validate_helper.validate_general_date])
            self.fields['ben_ward'] = forms.ChoiceField(label='Ward: ', choices=db_fieldchoices.get_list_of_wards())
            self.fields['community'] = forms.ChoiceField(label='Community (CWAC or GDCLSU): ', choices=db_fieldchoices.get_list_of_cwacs())
            self.fields['location_change_date'] = forms.DateField(widget=forms.HiddenInput(),required=False,label='', input_formats=validate_helper.date_input_formats_tpl, validators=[validate_helper.validate_general_date])
            
        self.fields['ben_district'] = forms.ChoiceField(label='District: ', choices=db_fieldchoices.get_list_of_districts())
        #self.fields['ben_ward'] = forms.ChoiceField(label='Ward: ', choices=db_fieldchoices.get_list_of_wards(districtid))
        #self.fields['community'] = forms.ChoiceField(label='Community (CWAC or GDCLSU): ', choices=db_fieldchoices.get_list_of_cwacs(wardid))
        self.fields['date_of_birth'] = forms.DateField(required=True, widget=DateInput('%d %B, %Y'), input_formats=('%d %B %Y', '%d %B, %Y',), validators=[validate_helper.validate_child_date_of_birth])
        
        self.helper = FormHelper()
                            
        self.helper.form_class = 'form-horizontal'
        self.helper.label_class = 'col-md-3'
        self.helper.attrs = {'padding-top':'3px'}
        self.helper.field_class = 'col-md-6'
        self.helper.form_action = actionurl
        self.helper.layout = Layout(
            MultiField(

                        header_text,
                        
                        Div(
                                Div(
                                    HTML('<p/>'),
                                    HTML("""<div id="div_id_beneficiary_id" class="form-group" style=\""""+do_display_status+"""\">
                                            <label for="id_beneficiary_id" class="control-label col-md-3">"""+settings.APP_NAME+""" beneficiary ID:</label>
                                            <div>
                                              <p class="form-control-static col-md-6"><strong>{{beneficiary.beneficiary_id}}</strong></p>
                                            </div>
                                            
                                        </div>"""),
                                    'birth_cert_num',
                                    Div('nrc', css_class='nrc_control'),
                                    css_class = 'panel-body pan',
                                    ),
                                Div(
                                Div(
                                    HTML('<input type="radio" name="edit_mode" id="id_edit_mode" value="1" checked="checked"/> Update beneficiary details'),
                                    css_class = 'panel-heading clearfix'
                                    ),
                                    css_class='panel panel-green',
                                    style = do_display_status,
                                    ),    
                                    
                                Div(HTML('Child identification information'),
                                    css_class = 'panel-heading'
                                    ),
                                Div(
                                    HTML('<p/>'),
                                    'first_name',
                                    'last_name',
                                    AppendedText('other_name','<span data-toggle="tooltip" title="Any other names which are either in official documents or are commonly used to refer to the person" data-placement="bottom"><i class="fa fa-info-circle"></i></span>'),
                                    'sex',
                                    AppendedText('date_of_birth','<span data-toggle="tooltip" title="If unknown, put estimate" data-placement="bottom"><i class="fa fa-info-circle"></i></span>'),
                                    'STEPS_OVC_number',
                                    #'steps_ovc_number',
                                    'pk_id',
                                    css_class = 'panel-body pan',
                                    ),
                                css_class='panel panel-pink',
                            ),
                        Div(
                                Div(HTML('Where the child lives'),
                                    Div(
                                            Div(
                                            HTML('<a href=\"/organisation/new/\" target=\"_blank\"><u>Register an organizational unit</u>                    .</a>'),
                                            HTML('Location change date: '),
                                            Field(Div('location_change_date', style='display:inline'), css_class='form-inline'),
                                            css_class = 'form-inline',
                                            style = do_display_status,
                                            ),
                                            Div(
                                                HTML('<a href=\"/organisation/new/\" target=\"_blank\"><u>Register an organizational unit</u></a>'),
                                                css_class = 'form-inline',
                                                style = dont_display_status,
                                                ),
                                        css_class= 'toolbars'
                                        ),
                                    css_class = 'panel-heading'
                                    ),
                            Div(
                                    HTML('<p/>'),
                                    Field(Div('edit_mode_hidden', style='visibility:hidden'), css_class='form-inline'),
                                    #Field(AppendedText('residential_institution','<span data-toggle="tooltip" title="If the child is a full time resident of an orphanage, care home, correctional institution or other type of residential institution" data-placement="bottom"><i class="fa fa-info-circle"></i></span>'), style="display:inline", css_class="autocompleteRes form-inline", id="id_guardian "),
                                    Div(Field('residential_institution', style="display:inline", css_class="autocompleteRes form-inline", id="id_guardian "),css_class='loc_controls'),
                                    css_class = 'panel-body pan',
                                    ),
                            Div(
                                HTML('<p/><strong>Or</strong>'),
                                style='margin-left:15%'
                                ),
                                Div(
                                    HTML('<p/>'),
                                    Div('ben_district',css_class='loc_controls'),
                                    Div('ben_ward',css_class='loc_controls'),
                                    Div('community',css_class='loc_controls'),
                                    Div('physical_address', css_class='loc_controls'),
                                    #Field(Div('is_update_p', style='visibility:hidden'), css_class='form-inline'),
                                    css_class = 'panel-body pan',
                                    ),
                                css_class='panel panel-grey',
                            ),       
                        Div(
                            Div(HTML('Child\'s guardians/primary caregivers'),
                                    Div(
                                        Div(
                                            HTML('<a href=\"/beneficiary/guardian/\" target=\"_blank\"><u>Register a guardian</u>                  .</a>'),
                                            HTML('Guardian change date: '),
                                            Field(Div('guardian_change_date', style='display:inline'), css_class='form-inline'),
                                            css_class = 'form-inline',
                                            style = do_display_status,
                                            ),
                                        Div(
                                            HTML('<a href=\"/beneficiary/guardian/\" target=\"_blank\"><u>Register a guardian</u></a>'),
                                            css_class = 'form-inline',
                                            style = dont_display_status,
                                            ),
                                        css_class= 'toolbars'
                                        ),
                                    css_class = 'panel-heading'
                                    ),
                                Div(
                                    Field(Div('guardians_hidden', style='visibility:hidden'), css_class='form-inline'),
                                    Div('no_adult_guardians', style='margin-left:17%'),
                                    #Div(Field(AppendedText('guardian','<span data-toggle="tooltip" title="Search by name or ID" data-placement="bottom"><i class="fa fa-info-circle"></i></span>'), style="display:inline", css_class="autocompleteben form-inline", id="id_guardian "), css_class='guardian_controls'),
                                    Div(Field('guardian', style="display:inline", css_class="autocompleteben form-inline", id="id_guardian "), css_class='guardian_controls'),
                                    Div('notes_relationship', css_class='guardian_controls'),
                                    HTML("<label id=\"guardian_error_display\" style=\"color:red;margin-left:26%;display:None\"><strong>  </strong></label>"),
                                    css_class = 'panel-body pan',
                                    ),
                                Div(
                                    HTML("<br><a type=\"button\" onclick=\"addGuardian()\" class=\"dontskip btn btn-success\">Add</a><br><br>"),
                                    style='margin-left:41%',
                                    css_class='guardian_controls'
                                    ),
                                Div(
                                    HTML("""
                                    <table id="guardianTable"  class="table table-hover table-condensed">
                                        <thead>
                                        <tr>
                                            <th>NRC or ZOMIS Beneficiary ID</th>
                                            <th>Name</th>
                                            <th>Notes on relationship with child</th>
                                            <th style=\""""+hide_status+"""\">Use contact details</th>
                                            <th>Remove</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        {% if beneficiary.guardian  %}
                                        {% for guardn in beneficiary.guardian %}
                                            <tr>
                                                <td id = "1">{{ guardn.guardian }}</td>
                                                <td id = "2">{{ guardn.guard_name }}</td>
                                                <td id = "3">{{ guardn.notes_relationship }}</td>
                                                <td id = "3"> </td>
                                                <td id = '5'> <button onclick='myDeleteFunction()' class='removebutton'><i class='fa fa-trash fa-2'></i></button></td>
                                            </tr>
                                        {% endfor %}
                                        {% endif %}
                                        </tbody>
                                    </table>
                                    
                                    
                                    
                                    <input id="ben_id_hidden" type="hidden">
                                    <input id="ben_name_hidden" type="hidden">
                                    <input id="ben_wfc_name_national_id" type="hidden">
                                    
                                    <input id="ben_res_name_hidden" type="hidden">
                                    <input id="ben_res_id_hidden" type="hidden">
                                    
                                    
                                    
                                    <script>
                                    
                                    
                                                                        
                                    function modifyChangeDateProp()
                                    {
                                        if(\""""+hide_status+"""\" == "visibility:hidden")
                                        {
                                            $("#id_guardian_change_date").prop('required', true);
                                        }
                                    }
                                    
                                    function addGuardian() {
                                            
                                            var enteredGuardian = $('input[name="guardian"]').val();
                                            var spVals = enteredGuardian.split(',');
                                            var len = spVals.length;
                                            
                                            if(enteredDataInvalid())
                                            {
                                                return;
                                            }
                                            
                                            if(len != 3)
                                            {
                                                return;
                                            }
                                            var ident = spVals[0];
                                            var guardian = spVals[2];
                                            var notes = $('input[name=notes_relationship]').val();
                                            
                                            if(enteredGuardianAlreadyExists(ident))
                                            {
                                                return;
                                            }
                                            
                                            var v = '"'+ident+'"';
                                            if(\""""+hide_status+"""\" == 'visibility:hidden')
                                            {
                                                $("#guardianTable").find('tbody')
                                                .append($('<tr>')
                                                    .append($('<td id="1">')
                                                        .append($('<label>')
                                                            .text(ident)
                                                        )
                                                    )
                                                    .append($('<td id="2">')
                                                        .append($('<label>')
                                                            .text(guardian)
                                                        )
                                                    )
                                                    .append($('<td id="3">')
                                                        .append($('<label>')
                                                            .text(notes)
                                                        )
                                                    )
                                                    .append($("<td id='4' > </td>"))
                                                    .append($("<td id='5'> <button onclick='myDeleteFunction()' class='removebutton'><i class='fa fa-trash fa-2'></i></button></td>"))
                                                );
                                            }
                                            else
                                            {
                                                $("#guardianTable").find('tbody')
                                                .append($('<tr>')
                                                    .append($('<td id="1">')
                                                        .append($('<label>')
                                                            .text(ident)
                                                        )
                                                    )
                                                    .append($('<td id="2">')
                                                        .append($('<label>')
                                                            .text(guardian)
                                                        )
                                                    )
                                                    .append($('<td id="3">')
                                                        .append($('<label>')
                                                            .text(notes)
                                                        )
                                                    )
                                                    .append($("<td id='4' > <button type='button' onclick='useContacts("+v+")' class='use-address'><i class='fa fa-check-square-o'></i></button></td>"))
                                                    .append($("<td id='5'> <button onclick='myDeleteFunction()' class='removebutton'><i class='fa fa-trash fa-2'></i></button></td>"))
                                                );
                                            }
                                            
                                            modifyChangeDateProp();
                                            updateTableData();   
                                            clearForm();
                                        }
                                        function clearForm(){
                                            $('input[name="guardian"]').val('');
                                            $('input[name="notes_relationship"]').val('');
                                            $('input[id=ben_id_hidden]').val('');;
                                            $('input[id=ben_name_hidden]').val('');
                                            $("#guardian_error_display").hide();
                                        }
                                        
                                        function enteredGuardianAlreadyExists(enteredGuardianId)
                                        {
                                            var alreadyEntered = false;
                                        
                                            $("#guardianTable tr").each(function(i, v){
                                                $(this).children('td').each(function(ii, vv){
                                                    if ($(this).attr("id") == '1'){
                                                        if($(this).text() == enteredGuardianId)
                                                        {
                                                            $("#guardian_error_display").text("This guardian has already been added");
                                                            $("#guardian_error_display").show();
                                                            alreadyEntered = true;
                                                        }                                          
                                                    }
                                                });
                                            })
                                            
                                            if(alreadyEntered)
                                            {
                                                 return true;
                                            }
                                            else
                                            {
                                                 return false;
                                            }
                                        }
                                        
                                        function enteredDataInvalid()
                                        {
                                            var enteredGuardian = $('input[name="guardian"]').val();
                                            var spVals = enteredGuardian.split(',');
                                            var len = spVals.length;
                                            if(len == 3)
                                            {
                                                if(spVals[0]!="" || spVals[0]=="")
                                                {
                                                    if(spVals[0]!="" && testbenId(spVals[0]))
                                                    {
                                                         return false;
                                                    }
                                                    $("#guardian_error_display").text("The entered guardian beneficiary Id is invalid.");
                                                    $("#guardian_error_display").show();
                                                    return true;
                                                    
                                                }
                                                else if(spVals[1]!=""||spVals[1]=="")
                                                {
                                                    if(spVals[1]!="" && testNrc(spVals[1]))
                                                    {
                                                        return false;
                                                    }
                                                    
                                                    $("#guardian_error_display").text("The entered guardian National Id is invalid.");
                                                    $("#guardian_error_display").show();
                                                    return true;
                                                }
                                                else if(spVals[2]=="")
                                                {
                                                    $("#guardian_error_display").text("Guardian Name cannot be empty.");
                                                     $("#guardian_error_display").show();
                                                     return true;
                                                }
                                            }
                                            else
                                            {
                                                $("#guardian_error_display").text("Please enter valid guardian details in the format beneficiary Id, National Id, Guardian Full Name.");
                                                 $("#guardian_error_display").show();
                                                 return true;
                                            }
                                            
                                            if($('input[name=notes_relationship]').val()=="")
                                            {
                                                $("#guardian_error_display").text("The notes on relationship field cannot be empty");
                                                 $("#guardian_error_display").show();
                                                 return true;
                                            }
                                        
                                        return false;
                                        }
                                        
                                        function testNrc(nrcNum) { 
                                          // http://stackoverflow.com/a/46181/11236
                                                      
                                            var re = /^\b\d{6}\/\b\d{2}\/\b[1-3]{1}$/;
                                            return re.test(benId);
                                        }
                                        
                                        function testbenId(nrcNum) { 
                                            // http://stackoverflow.com/a/46181/11236
                                            
                                            var re = /^B\d{8}$/;
                                            return re.test(nrcNum);
                                        }
                                        
                                        function useContacts(guard_id){
                                             $.ajax({
                                                        dataType: "json",
                                                        url: "/beneficiary/guardian_contact?guardian_id=" + guard_id,
                                                        contentType: "application/json; charset=utf-8",
                                                        success: function (reg_guardians) {
                                                            $.each(reg_guardians, function (key, value) {
                                                                switch(key)
                                                                {
                                                                    case 'Phone number - mobile':
                                                                            $( "#id_mobile_phone_number" ).val(value);
                                                                            break;
                                                                    case 'Email address':
                                                                            $( "#id_email_address" ).val(value);
                                                                            break;
                                                                    case 'Physical address':
                                                                            $( "#id_physical_address" ).val(value);
                                                                            break;
                                                                    default:
                                                                            break;
                                                                }
                                                            });
                                                        }
                                                    });
                                        }
                                        
                                        function myDeleteFunction(){
                                            $(document).on('click', 'button.removebutton', function () { 
                                                $(this).closest('tr').remove();
                                                return false;
                                            });
                                            modifyChangeDateProp();
                                        }
                                        
                                        function updateTableData(){
                                        
                                        var row_count = $('#guardianTable tbody').children().length;
                                        
                                        if(row_count <= 0)
                                            {
                                                return;
                                            }                                            
                                                                                        
                                            var data = Array();
                                            $("#guardianTable tr").each(function(i, v){
                                                data[i] = Array();
                                                $(this).children('td').each(function(ii, vv){
                                                    if ($(this).attr("id") == '2' || $(this).attr("id") == '4' || $(this).attr("id") == '5' || $(this).text() == ''){
                                                        //do nothing really                                                      
                                                    }
                                                    else{                                              
                                                        data[i][ii] = $(this).text();
                                                    }
                                                    
                                                    if($(this).attr("id") == '5')
                                                    {
                                                        data[i][ii] = '#';
                                                    }
                                                }); 
                                            })
                                            
                                            $( "#id_guardians_hidden" ).val('');
                                            
                                            $( "#id_guardians_hidden" ).val(data);
                                            
                                            //$( "#res_id_hidden" ).val($("#id_ben_res_id_hidden").val());
                                        }
                                        
                                        function updateFinale(){
                                            checkUpdateMode();                                            
                                            FinalizeGuardianData();
                                            
                                            if($("input[id='id_no_adult_guardians']").is( ":checked" ))
                                            {
                                                $("#guardianTable").find('tbody').empty();
                                                $('input[name="guardians_hidden"]').val('');
                                                clearForm();
                                            }
                                            
                                            checkNrc();
                                        }
                                        
                                        function checkNrc()
                                        {
                                            if (!$("#id_nrc").prop('required'))
                                            {
                                                $("#id_nrc").val('');
                                            }
                                        }
                                        
                                        function FinalizeGuardianData(){
                                            if($('input[name="guardians_hidden"]').val() == "first_time")
                                            {
                                                updateTableData();
                                                if($('input[name="guardians_hidden"]').val() == "first_time")
                                                {
                                                    $('input[name="guardians_hidden"]').val('');
                                                }
                                            }
                                        }
                                        
                                        function checkUpdateMode(){
                                            var edit_mode = $('input:radio[name=edit_mode]:checked').val();
                                            $( "#id_edit_mode_hidden" ).val(edit_mode);                                       
                                        }
                                        
                                    </script>
                                  """),
                                    css_class = 'panel-body pan guardian_controls'
                                    ),
                                css_class='panel panel-pink',
                            ),
                       Div(
                                Div(HTML('Adverse conditions (Click as many as apply, either now or in the past)'),
                                    css_class = 'panel-heading',
                                    style = dont_display_status,
                                     ),
                                Div(
                                    HTML('<p/>'),
                                    Field('adverse_cond', template='ovc_main/adverse_conditions.html'),
                                    css_class = 'panel-body pan'
                                    ),
                                css_class='panel panel-grey',
                                style = dont_display_status,
                            ),
                        Div(
                                Div(HTML('Child contact details'),
                                    css_class = 'panel-heading'
                                    ),
                                
                                Div(
                                    HTML('<p/>'),
                                    Field(Div('prev_guard_change_date', style='visibility:hidden'), css_class='form-inline'),
                                    'mobile_phone_number',
                                    'email_address',
                                    Field(Div('prev_loc_change_date', style='visibility:hidden'), css_class='form-inline'),
                                    css_class = 'panel-body pan',
                                    ),
                                css_class='panel panel-pink',
                                
                            ),
                       Div(
                                Div(HTML('<input type="radio" name="edit_mode" id="id_edit_mode" value="2"/> Died'),
                                    css_class = 'panel-heading clearfix',
                                    style = do_display_status,
                                     ),
                                Div(
                                    HTML('<p/>'),
                                    Div('date_of_death', css_class='dead_control'),
                                    css_class = 'panel-body pan'
                                    ),
                                css_class='panel panel-green',
                                style = do_display_status,
                            ),
                       Div(
                                Div(HTML('<input type="radio" name="edit_mode" id="id_edit_mode" value="3"/> OVC never existed (was a registration mistake or a duplicate)'),
                                    css_class = 'panel-heading clearfix',
                                    style = do_display_status,
                                    ),
                                css_class='panel panel-green',
                            ),
                        Div(
                                Div(HTML('Paper Trail'),
                                    Div(
                                        Div(
                                            HTML('<a href=\"/workforce/new_workforce/\" target=\"_blank\"><u>Register a workforce member</u></a>'),
                                            css_class = 'form-inline',
                                            style = dont_display_status,
                                            ),
                                        css_class= 'toolbars'
                                    ),
                                    css_class = 'panel-heading'
                                    ),
                                
                                Div(
                                    HTML('<p/>'),
                                    #Field(AppendedText('workforce_member','<span data-toggle="tooltip" title="Search by name or ID" data-placement="bottom"><i class="fa fa-info-circle"></i></span>'), style="display:inline", css_class="autocompletewfc form-inline", id="id_guardian "),
                                    Field('workforce_member', style="display:inline", css_class="autocompletewfc form-inline", id="id_guardian "),
                                    'date_paper_form_filled',
                                    Field(Div('res_id_hidden', style='visibility:hidden'), css_class='form-inline'),
                                    css_class = 'panel-body pan'
                                    ),
                                Div(
                                    Div(
                                    HTML("""
                                    <table id="paperTrailTable"  class="table table-hover table-bordered">
                                        <thead>
                                        <tr>
                                            <th>Person recorded on paper</th>
                                            <th>Paper date</th>
                                            <th>Person recorded electronically</th>
                                            <th>Electronic date time</th>
                                            <th>Interface</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        {% if beneficiary.audit_trail  %}
                                        {% for audit in beneficiary.audit_trail %}
                                            <tr>
                                                <td id = "1">{{ audit.Person_recorded_on_paper }}</td>
                                                <td id = "2">{{ audit.Paper_date }}</td>
                                                <td id = "3">{{ audit.Person_recorded_electronically }}</td>
                                                <td id = "4">{{ audit.Electronic_date_time }}</td>
                                                <td id = "5">{{ audit.Interface }}</td>
                                            </tr>
                                        {% endfor %}
                                        {% endif %}
                                        </tbody>
                                    </table>
                                    
                                                                         
                                    <input id="ben_wfc_name_hidden" type="hidden">
                                    <input id="ben_wfc_id_hidden" type="hidden">
                                    <input id="ben_wfc_name_national_id" type="hidden">
                                  """),
                                    )
                                    ),
                                    css_class='mandatory panel panel-grey',
                          ),
                       Div(
                        HTML("<Button onclick=\"updateFinale()\" type=\"submit\" style=\"margin-right:4px\" class=\"mandatory btn btn-primary\"><i class=\"fa fa-floppy-o\"></i> "+submitButtonText+"</Button>"),
                        HTML("<a type=\"button\" href=\"/beneficiary/ben_search/\"class=\"mandatory btn btn-success\"><i class=\"fa fa-undo\"></i> Cancel</a><br><br>"),
                        style='margin-left:38%'
                        ),
            )
        )
        
        if self.is_update_page:
            layout = self.helper.layout
            searchitems = filter_fields_for_display(self.user, permission_type_fields)
            print searchitems, 'search items'
            if searchitems:
                del_form_field(layout, searchitems)
                self.helper.layout = layout
    
    
      
    def clean(self):
        if (self.cleaned_data.get('guardian_change_date') != None):
            entered_value = self.cleaned_data.get('guardian_change_date')
            if (self.cleaned_data.get('prev_guard_change_date') != None):
                date_linked_value = self.cleaned_data.get('prev_guard_change_date')
                if entered_value < date_linked_value:
                    raise ValidationError("Guardian change date cannot be less than the the date linked")
        
        if (self.cleaned_data.get('location_change_date') != None):
            entered_value = self.cleaned_data.get('location_change_date')
            pk_pers = self.cleaned_data.get('pk_id')
            most_recent_change_date = None
            
            if pk_pers:
                most_recent_change_date = db_fieldchoices.get_most_recent_record_date_for_person_id(pk_pers)
            
            print 'most_recent_change_dateX', most_recent_change_date
            if (self.cleaned_data.get('prev_loc_change_date') != None):
                date_linked_value = self.cleaned_data.get('prev_loc_change_date')
                if entered_value < date_linked_value:
                    raise ValidationError("Location change date cannot be less than the the date linked")
                elif entered_value < most_recent_change_date:
                    self._errors["location_change_date"] = self.error_class([u"Location change date cannot be less than previous record dates for this OVC"])
        
        if (self.cleaned_data.get('guardians_hidden') != None):
            value = self.cleaned_data.get('guardians_hidden')
            no_guardian = self.cleaned_data.get('no_adult_guardians')
            
            if not no_guardian:
                if not value:
                        raise ValidationError("At least one guardian must be chosen unless the tick box \"no adult guardians\" is chosen")
        
        if (self.cleaned_data.get('nrc') != None or self.cleaned_data.get('first_name') != None or self.cleaned_data.get('last_name') != None):
            
            nrc_id = self.cleaned_data.get('nrc')
            f_name = self.cleaned_data.get('first_name')
            l_name = self.cleaned_data.get('last_name')
            
            if nrc_id or (f_name and l_name):
                if not self.is_update_page:
                                        
                    if(RegPerson.objects.filter(national_id=nrc_id,is_void=False,date_of_death = None).count() > 0):
                        person = RegPerson.objects.filter(national_id=nrc_id,is_void=False,date_of_death = None)
                        m_person = None
                        for m_per in person:
                            m_person = m_per
                        
                        if m_person.national_id:
                            msg = "["+m_person.national_id+"],["+m_person.beneficiary_id+"], ["+m_person.first_name+", "+m_person.surname+", "+m_person.other_names+"] is already in this registry."
                        else:
                            msg = "This NRC is already in the system. You cannot register this NRC again."
                        if m_person.national_id:    
                            raise ValidationError(msg)
                    
                    if(RegPerson.objects.filter(first_name=f_name,surname=l_name,is_void=False,date_of_death = None).count() > 0):
                        person = RegPerson.objects.filter(first_name=f_name,surname=l_name,is_void=False,date_of_death = None)
                        m_person = None
                        for m_per in person:
                            m_person = m_per
                        
                        if m_person:
                            msg = "["+m_person.national_id+"],["+m_person.beneficiary_id+"], ["+m_person.first_name+", "+m_person.surname+", "+m_person.other_names+"] is already in this registry."# Do you want to save?"
                        else:
                            msg = "This person is already in the system."
                        raise ValidationError(msg)
        
                               
        if not self.is_update_page:
            adverse_cond = self.cleaned_data.get('adverse_cond')
            if not adverse_cond:
                self._errors["adverse_cond"] = self.error_class([u"Please select at least one adverse condition for this child"])
                
        if self.is_update_page:
            if (self.cleaned_data.get('date_of_birth') != None):
                dob = self.cleaned_data.get('date_of_birth')
                if (self.cleaned_data.get('date_of_death') != None):
                    dod = self.cleaned_data.get('date_of_death')
                    if dod:
                        if dod < dob:
                            self._errors["date_of_death"] = self.error_class([u"Date of death cannot be less than date of birth"])
            
        return self.cleaned_data

    
    
