from django.core.exceptions import ValidationError
from datetime import date,timedelta
from ovc_main.models import RegPerson
from ovc_main.utils.fields_list_provider import calculate_age

mobile_phone_number_regex = r'^\+260[1-9]{9,9}$|^0[1-9]{9,9}$'
date_input_format  = '%d-%B-%Y'
date_input_formats_tpl = ('%d-%B-%Y',)

def validate_child_date_of_birth(value):
    age = calculate_age(value)
    if not value:
        raise ValidationError("The date of birth cannot be empty")
    elif value > date.today():
        raise ValidationError("The entered date of birth is in the future")
    elif age > 18:
        raise ValidationError("OVC must be less than 18 years old.")
    
def validate_guardian_date_of_birth(value):
    age = calculate_age(value)
    if not value:
        raise ValidationError("The date of birth cannot be empty")
    elif value > date.today():
        raise ValidationError("The entered date of birth is in the future")
    elif age < 18:
        raise ValidationError("Guardian must be 18 years or older.")
    elif age > 100:
        raise ValidationError("Age cannot be greater than 100 years")
    
def validate_general_date(value):
    if value > date.today():
        raise ValidationError("The entered date is in the future")

def validate_date_of_birth(value):
    #days_in_year = 365.2425    
    #age = int((date.today() - value).days / days_in_year)
    age = calculate_age(value)
    #print age,'age'
    if value:
        if date.today() < value:
            raise ValidationError("This date is in the future")
        elif age < 18 and age >= 0:
            raise ValidationError("This date of birth indicates the workforce member/user is less than 18 years old. Please check the date again.")
        elif age > 100:
            raise ValidationError("This date of birth indicates the workforce member/user is greater than 100 years old. Please check the date again.")
        elif age == -1:
            raise ValidationError("Please enter a valid date.")

def deny_future_dates(value):
    #print value,'value'
    #print date.today(),'date.today()'
    #print date.today() < value,'date.today() < value'
    if value:
        if date.today() < value:
            raise ValidationError("This date is in the future")
        
def validate_org_unit(value):
    if value:
        no_of_primary_org_units = 0
        #,,org_id:U0000018,primary_org:Yes,wra:No,,org_id:U0000034,primary_org:No,wra:No
        orgs = value.split(',,')
        #Check how many primary org units are registered
        for org in orgs:
            attrs = org.split(',')
            for attr in attrs:
                kvp = attr.split(':')
                if len(kvp) > 1:
                    if kvp[0] == 'primary_org' and kvp[1] == 'Yes':
                        no_of_primary_org_units+=1
        #if 0 then V eror
        if no_of_primary_org_units == 0:
            raise ValidationError("Please set one primary organisation unit for this person. No primary organisation seleted.")
        #if >1 then V error
        if no_of_primary_org_units > 1:
            raise ValidationError("Please set only one primary organisation unit for this person. %d primary organisation units were seleted." % no_of_primary_org_units)

'''
def check_nrc_for_dups(value):
    print value,'National Registration ID'
    if(RegPerson.objects.filter(national_id=value,is_void=False,date_of_death = None).count() > 0):
        print 'lalalala'
        m_person = RegPerson.objects.get(national_id=value,is_void=False,date_of_death = None)
        print m_person.first_name
        raise ValidationError(
            "This NRC is already linked to "+m_person.first_name+" "+m_person.other_names+" "+m_person.surname+". You cannot register this NRC again."
        )
'''