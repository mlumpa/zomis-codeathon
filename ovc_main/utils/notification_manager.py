from django.core.mail import send_mail
from ovc_auth.user_manager import default_password
from ovc_main.models import RegPersonsContact
from ovc_main.utils import lookup_field_dictionary as lookup

def send_notification(user_model=None, new_user = None, message_type=None, contact_type = None):
    if message_type:
        if message_type == MessageType.NEW_REGISTRATION:
            password = default_password
            email_address = ''
            phone_number = ''
            user_name = ''
            if len(new_user.workforce_id) == 8:
                user_name = new_user.workforce_id
            else:
                user_name = new_user.national_id
            if contact_type == ContactType.EMAIL:
                if len(RegPersonsContact.objects.filter(person=user_model,contact_detail_type_id=lookup.contact_email_address,is_void=False)) > 0:
                    email_address = (RegPersonsContact.objects.get(person=user_model,contact_detail_type_id=lookup.contact_email_address,is_void=False)).contact_detail
                    message = 'You have been successfully registered. You username is '+user_name+' and password is '+password+'. Go to http:// to login.'
                    try:
                        send_mail('Registration successfull.', message, 'glyoko@gmail.com', [email_address], fail_silently=False)
                    except Exception as e:
                        print e 
                        raise Exception('Failed to send an email to the new user')
                    return True
            

     
#An attempt to simulate enums. we only get enums in python 3.4 :(
class MessageType:
    NEW_REGISTRATION = 1
    PASSWORD_RESET = 2
    SMS_DELIVERY_CONFIRMATION = 3
    
class ContactType:
    SMS = 1
    EMAIL = 2
    ALL = 3