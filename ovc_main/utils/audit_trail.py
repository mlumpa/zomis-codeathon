from ovc_main.models import  RegOrgUnitsAuditTrail as org_unit_logger, RegPersonsAuditTrail as person_logger
from datetime import datetime

def log(name,model_to_log,transaction_type_id,interface_id, date_recorded_paper = None, person_id_recorded_paper=None, person_electronic = None):
    if not model_to_log:
        raise Exception('Model could not be resolved')
    if not transaction_type_id:
        raise Exception('Transaction type could not be resolved')
    if not interface_id:
        raise Exception('Interface type could not be resolved')

    if name == 'person':
        person_logger.objects.create(person_id=model_to_log.pk, 
                                     transaction_type_id = transaction_type_id, 
                                     interface_id = interface_id,  
                                     date_recorded_paper = date_recorded_paper, 
                                     person_id_recorded_paper = person_id_recorded_paper, 
                                     person_id_modified = person_electronic
                                     )
        
    if name == 'organisation':
        org_unit_logger.objects.create(org_unit_id=model_to_log.pk, transaction_type_id = transaction_type_id, interface_id = interface_id,person_id_modified = person_electronic)
        