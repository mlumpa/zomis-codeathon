def get_list_from_dic_or_querydict(obj, key):
    from django.http import QueryDict
    if not key in obj:
        return []
    if type(obj) == dict:
        return obj[key]
    if type(obj) == QueryDict:
        return obj.getlist(key)
    
def convert_to_int_array(valuetoconvert):
    if str(valuetoconvert).isdigit():
        return [int(valuetoconvert)]
    elif valuetoconvert:
        tmp = map(int, valuetoconvert)
        return tmp
    else:
        return None
    
def list_has_key(list_to_check, key_value):
    #Check if a given key exists in a tuple
    if key_value in list_to_check:
        return True
    return False

