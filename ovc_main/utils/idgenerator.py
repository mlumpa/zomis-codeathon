'''
Created on Sep 23, 2014

@author: MLumpa
'''

organisation_id_prefix = 'U'
benficiary_id_prefix = 'B'
workforce_id_prefix = 'W'

def organisation_id_generator(modelid):
    uniqueid = '%05d' % modelid
    checkdigit = calculate_luhn(str(uniqueid))
    
    return organisation_id_prefix + str(uniqueid) + str(checkdigit)

def beneficiary_id_generator(modelid):
    uniqueid = '%07d' % modelid
    checkdigit = calculate_luhn(str(uniqueid))
    
    return benficiary_id_prefix + str(uniqueid) + str(checkdigit)

def workforce_id_generator(modelid):
    uniqueid = '%06d' % modelid
    checkdigit = calculate_luhn(str(uniqueid))
    
    return workforce_id_prefix + str(uniqueid) + str(checkdigit)

def luhn_checksum(card_number):
    '''
    http://en.wikipedia.org/wiki/Luhn_algorithm
    '''
    def digits_of(n):
        return [int(d) for d in str(n)]
    digits = digits_of(card_number)
    odd_digits = digits[-1::-2]
    even_digits = digits[-2::-2]
    checksum = 0
    checksum += sum(odd_digits)
    for d in even_digits:
        checksum += sum(digits_of(d*2))
    return checksum % 10
 
def is_luhn_valid(card_number):
    '''
    http://en.wikipedia.org/wiki/Luhn_algorithm
    '''
    return luhn_checksum(card_number) == 0
    

def calculate_luhn(partial_card_number):
    '''
    http://en.wikipedia.org/wiki/Luhn_algorithm
    '''
    check_digit = luhn_checksum(int(partial_card_number) * 10)
    return check_digit if check_digit == 0 else 10 - check_digit