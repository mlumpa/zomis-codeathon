#workforce
workforce_type_cat = 'Person type (workforce member)'
workforce_type = 'Person type (user/workforce member)'
workforce_type_ngo_id = 'TWNE'
workforce_type_gov_id = 'TWGE'
workforce_type_vol_id = 'TWVL'

steps_ovc_caregiver = 'ISOV'
govt_man_number  = 'IMAN'
police_sign_number = 'IPCS'
teacher_service_id = 'IDTS'
empty_workforce_id = 'N/A'

#general
sex = 'Sex'
gen_YesNo = 'General YesNo'
dist = 'district'
ward = 'ward'

#organisation
org_unit_type = 'Organisational unit type'
org_unit_type_res_Institution = 'TNRS'
gdclsu_text = 'TNCW'#CWAC

contact_phone_number = 'CPHL'
contact_mobile_phone = 'CPHM'
contact_designated_mobile_phone = 'CPHD'
contact_email_address = 'CEMA'
contact_post_address = 'CPOA'
contact_physical_address = 'CPHA'
contact_landline_phone = 'CPHL'


geo_type_district = 'district'
geo_type_province = 'province'
geo_type_constituency = 'constituecy'
geo_type_ward = 'ward'
geo_type_community = 'community'
geo_type_residential_institution = 'residential'

#beneficiary
adverse_condition = 'Adverse condition'
child_beneficiary_ovc = 'TBVC'
child_guardian_no_guardian = 'noguardian'
child_guardian_guardian = 'TBGR'
child_guardian_notes_relationship = 'notesrelationship'
beneficiary_category = 'Person type (beneficiary)'

paper_trail_wfc = ''

#general
web_interface_id = 'INTW'
sms_interface_id = 'INTS'
capture_interface_id = 'INTC'

register_org_unit = 'REGU'
register_user = 'REGS'
register_workforce_member = 'REGW'

update_beneficiary = 'UPDB'
update_org_unit = 'UPDU'
update_user = 'UPDS'
update_workforce_member = 'UPDW'

register_beneficiary = 'REGB'
update_beneficiary = 'UPDB'

org_type_community_based_org = 'TNCB'
org_type_community_level_committee = 'TNCM'
org_type_cwac = 'TNCW'
org_type_district_level_committee = 'TNCD'
org_type_district_office_gov = 'TNGD'
org_type_national_level_gov = 'TNGN'
org_type_ngo_private_national_hq = 'TNNH'
org_type_ngo_private_single_district ='TNND'
org_type_ngo_private_multiple_district = 'TNNM'
org_type_provincial_level_committee = 'TNCP'
org_type_provincial_office_gov ='TNGP'
org_type_residential_children = 'TNRS'
org_type_sub_district_gov ='TNGS'

legal_org_registration_type_ngo_number = 'INGO'

#from django.contrib.auth.models import Group
download_sections = {
    'all': {
        'precedence':-1,
        'title':'All sections', 
        'models': []
        },
    'DSPR': {
        'title':'Person registry', 
        'precedence':4,
        'models': [
            {'id':'persons', 'model':'RegPerson','serializer':'RegPersonSerializer'},
            {'id':'persons_geo', 'model':'RegPersonsGeo','serializer':'RegPersonsGeoSerializer'},
            {'id':'persons_gdclsu', 'model':'RegPersonsGdclsu','serializer':'RegPersonsGdclsuSerializer'},
            {'id':'persons_orgs', 'model':'RegPersonsOrgUnits','serializer':'RegPersonsOrgUnitsSerializer'},
            {'id':'persons_cat', 'model':'RegPersonsTypes','serializer':'RegPersonsTypesSerializer'}
            ]
        },
    'DSOR': {
        'title': 'Organisational registry', 
        'precedence':3,
        'models': [
            {'id':'org_unit', 'model':'RegOrgUnit','serializer':'RegOrgUnitSerializer'},
            {'id':'org_unit_geo', 'model':'RegOrgUnitGeography','serializer':'RegOrgUnitGeoSerializer'}
            ]
        },
    'DSPA': {
        'title': "Person access", 
        'precedence':5,
        'models': [
            #{'id':'users', 'model':'AppUser','serializer':''},
            #{'id':'user_roles', 'model':'auth_user_groups','serializer':''}
            ]#TODO use actual model
        },
    'DSSS': {
        'title': 'Security setup', 
        'precedence':2,
        'models': [
            #{'id':'role_perms', 'model':'auth_group_permissions','serializer':''}
            ]#TODO use actual model
        }, 
    'DSLT': {
        'title':'Lists', 
        'precedence':1,
        'models': [
            {'id':'list_gen', 'model':'SetupList','serializer':'SetupListSerializer'},
            {'id':'list_geo', 'model':'SetupGeorgraphy','serializer':'SetupGeoSerializer'},
            #{'id':'list_questions', 'model':'ListQuestions','serializer':''},
            #{'id':'list_answers', 'model':'ListAnswers','serializer':''},
            #{'id':'list_perms', 'model':'OVCPermission','serializer':''},
            #{'id':'roles', 'model':'Group','serializer':''}
            ]
        }
    }
