from django.db import models
from datetime import datetime
# Create your models here.
class RegOrgUnit(models.Model):
    unit_id = models.CharField(max_length=12)
    unit_name = models.CharField(max_length=255)
    unit_type_id = models.CharField(max_length=4)
    is_gdclsu  = models.BooleanField()
    date_operational = models.DateField(null=True)
    date_closed = models.DateField(null=True)
    is_void = models.BooleanField(default=False)
    parent_org_unit_id = models.CharField(max_length=12, null=True, blank=True)

    
    def _is_active(self):
        if self.date_closed:
            return False
        else:
            return True
    
    is_active = property(_is_active)
    
    class Meta:
        db_table = 'tbl_reg_org_units'
        
    def make_void(self, date_closed=None):
        self.is_void = True
        if date_closed:
            self.date_closed = date_closed
        super(RegOrgUnit, self).save()
    
    
    
class RegOrgUnitContact(models.Model):
    #organisation_unit_id = models.CharField(max_length=7)
    organisation = models.ForeignKey(RegOrgUnit)
    contact_detail_type_id = models.CharField(max_length=20)
    contact_detail = models.CharField(max_length=255)
    is_void = models.BooleanField(default=False)
    class Meta:
        db_table = 'tbl_reg_org_units_contact'
    
class RegOrgUnitExternalID(models.Model):
    #organisation_unit_id = models.CharField(max_length=7)
    organisation = models.ForeignKey(RegOrgUnit)
    identifier_type_id = models.CharField(max_length=4)
    identifier_value = models.CharField(max_length=255, null=True)
    is_void = models.BooleanField(default=False)
    
    class Meta:
        db_table = 'tbl_reg_org_units_external_ids'
    
class RegOrgUnitGdclsu(models.Model):
    #organisation_unit_id = models.CharField(max_length=7)
    organisation = models.ForeignKey(RegOrgUnit)
    gdclsu_id = models.CharField(max_length=12)
    date_linked = models.DateField(null=True)
    date_delinked = models.DateField(null=True)
    is_void = models.BooleanField(default=False)
    
    class Meta:
        db_table = 'tbl_reg_org_units_gdclsu'
    
class RegOrgUnitGeography(models.Model):
    #org_unit_id = models.CharField(max_length=7)
    organisation = models.ForeignKey(RegOrgUnit)
    area_id = models.IntegerField()
    date_linked = models.DateField(null=True)
    date_delinked = models.DateField(null=True)
    is_void = models.BooleanField(default=False)
    
    class Meta:
        db_table = 'tbl_reg_org_units_geo'
        
    def make_void(self, date_delinked=None):
        self.is_void = True
        if date_delinked:
            self.date_delinked = date_delinked
        elif not self.date_delinked:
            self.date_delinked = datetime.now().date()
        super(RegOrgUnitGeography, self).save()
    
class RegPerson(models.Model):
    #person_id = models.CharField(primary_key=True,max_length=32)
    beneficiary_id = models.CharField(max_length=10, null=True, blank=True)
    workforce_id = models.CharField(max_length=8, null=True, blank=True)
    birth_reg_id = models.CharField(max_length=15, null=True, blank=True)
    national_id = models.CharField(max_length=15, null=True, blank=True)
    first_name = models.CharField(max_length=255)
    other_names = models.CharField(max_length=255, null=True, blank=True)
    surname = models.CharField(max_length=255)
    date_of_birth = models.DateField()
    date_of_death = models.DateField(null=True, blank=True)
    sex_id = models.CharField(max_length=4)
    is_void = models.BooleanField(default=False)
    
    def _get_full_name(self):
        return self.first_name +' '+ self.other_names + ' ' + self.surname
    
    def make_void(self):
        self.is_void = True
        super(RegPerson,self).save()
    
    def record_death(self,date_of_death=None):
        if date_of_death:
            self.date_of_death = date_of_death
        super(RegPerson,self).save()
        
    full_name  = property(_get_full_name)
    class Meta:
        db_table = 'tbl_reg_persons'   
    
class SetupGeorgraphy(models.Model):
    area_id = models.IntegerField()
    area_type_id = models.CharField(max_length=50)
    area_name = models.CharField(max_length=100)
    parent_area_id = models.IntegerField(null=True)
    area_name_abbr = models.CharField(max_length=5, null=True)
    timestamp_created = models.DateTimeField(auto_now_add=True, default=datetime.now())
    timestamp_updated = models.DateTimeField(auto_now=True, default=datetime.now())
    is_void = models.BooleanField(default=False)
    class Meta:
        db_table = 'tbl_list_geo'
    
class SetupList(models.Model):
    item_id = models.CharField(max_length=4)
    item_description = models.CharField(max_length=255)
    item_description_short = models.CharField(max_length=26, null=True)
    item_category = models.CharField(max_length=255, null=True, blank=True)
    the_order = models.IntegerField(null=True)
    user_configurable = models.BooleanField(blank=True)
    sms_keyword = models.BooleanField(blank=True)
    is_void = models.BooleanField(default=False)
    field_name = models.CharField(max_length=200, null=True, blank=True)
    class Meta:
        db_table = 'tbl_list_general'

class AdminPreferences(models.Model):
    person = models.ForeignKey(RegPerson)
    preference_id = models.CharField(max_length=4)
    
    class Meta:
        db_table = 'tbl_admin_preferences'


class RegPersonsGuardians(models.Model):
    child_person = models.ForeignKey(RegPerson)
    guardian_person_id = models.CharField(max_length=10)
    relationship_notes = models.CharField(max_length=255)
    date_linked = models.DateField(null=True)
    date_delinked = models.DateField(null=True)
    is_void = models.BooleanField(default=False)
    
    class Meta:
        db_table = 'tbl_reg_persons_guardians'
    
class RegPersonsTypes(models.Model):
    person = models.ForeignKey(RegPerson)
    person_type_id = models.CharField(max_length=4)
    date_began = models.DateField(null=True)
    date_ended = models.DateField(null=True)
    is_void = models.BooleanField(default=False)
    
    def make_void(self,person_type_change_date=None):
        self.is_void = True
        if person_type_change_date:
            if person_type_change_date < self.date_began:
                person_type_change_date = datetime.now().date()
            self.date_ended = person_type_change_date
        super(RegPersonsTypes,self).save()
        
    class Meta:
        db_table = 'tbl_reg_persons_types'
    
class RegPersonsGeo(models.Model):
    person = models.ForeignKey(RegPerson)
    area_id = models.IntegerField()
    date_linked = models.DateField(null=True)
    date_delinked = models.DateField(null=True)
    is_void = models.BooleanField(default=False)
    
    def make_void(self,date_delinked, is_void):
        if date_delinked:
            if date_delinked < self.date_linked:
                date_delinked = datetime.now().date()
        self.date_delinked=date_delinked
        self.is_void=True
        super(RegPersonsGeo,self).save()
        
    class Meta:
        db_table = 'tbl_reg_persons_geo'

class RegPersonsGdclsu(models.Model):
    person = models.ForeignKey(RegPerson)
    gdclsu = models.ForeignKey(RegOrgUnit)
    date_linked = models.DateField(null=True)
    date_delinked = models.DateField(null=True)
    is_void = models.BooleanField(default=False)
    
    def make_void(self,date_delinked, is_void=True):
        if date_delinked:
            if date_delinked < self.date_linked:
                date_delinked = datetime.now().date()
        self.date_delinked=date_delinked
        self.is_void=True
        super(RegPersonsGdclsu,self).save()
        
    class Meta:
        db_table = 'tbl_reg_persons_gdclsu'
        
class RegPersonsExternalIds(models.Model):
    person = models.ForeignKey(RegPerson)
    identifier_type_id = models.CharField(max_length=4)
    identifier = models.CharField(max_length=255)
    is_void = models.BooleanField(default=False)
    
    def make_void(self):
        self.is_void = True
        super(RegPersonsExternalIds,self).save()
        
    class Meta:
        db_table = 'tbl_reg_persons_external_ids'

class RegPersonsContact(models.Model):
    person = models.ForeignKey(RegPerson)
    contact_detail_type_id = models.CharField(max_length=4)
    contact_detail = models.CharField(max_length=255)
    is_void = models.BooleanField(default=False)
    
    def make_void(self):
        self.is_void = True
        super(RegPersonsContact,self).save()

    class Meta:
        db_table = 'tbl_reg_persons_contact'

class RegPersonsOrgUnits(models.Model):
    person = models.ForeignKey(RegPerson)
    parent_org_unit = models.ForeignKey(RegOrgUnit)
    primary = models.IntegerField()
    date_linked = models.DateField(null=True)
    date_delinked = models.DateField(null=True)
    is_void = models.BooleanField(default=False)
    
    def make_void(self,parent_org_change_date=None):
        self.is_void = True
        if parent_org_change_date:
            if parent_org_change_date < self.date_linked:
                parent_org_change_date = datetime.now().date()
            self.date_delinked = parent_org_change_date
        super(RegPersonsOrgUnits,self).save()
        
    class Meta:
        db_table = 'tbl_reg_persons_org_units'
        
class CoreAdverseConditions(models.Model):
    beneficiary_id = models.CharField(max_length=15)
    adverse_condition_id = models.CharField(max_length=4)
    is_void = models.IntegerField(null=True)
    sms_id = models.IntegerField(null=True)
    form_id = models.IntegerField(null=True)
    
    class Meta:
        db_table = 'tbl_core_adverse_conditions'
        
class FormResChildren(models.Model):
    child_beneficiary_id = models.CharField(max_length=15)
    institution_id = models.CharField(max_length=12)
    form_id = models.CharField(max_length=128, null=True)
    sms_id = models.IntegerField(null=True)
    residential_status = models.CharField(max_length=4, null=True)
    
    class Meta:
        db_table = 'tbl_form_res_children'
        
class RegOrgUnitsAuditTrail(models.Model):
    transaction_id = models.AutoField(primary_key=True)
    org_unit_id = models.CharField(max_length=8, null=True, db_index=True)
    transaction_type_id = models.CharField(max_length=4, null=True, db_index=True)
    interface_id = models.CharField(max_length=4, null=True, db_index=True)
    timestamp_modified = models.DateTimeField(auto_now_add=True)
    person_id_modified = models.IntegerField(null=True, db_index=True)
    
    class Meta:
        db_table = 'tbl_reg_org_units_audit_trail'
    
    
class RegPersonsAuditTrail(models.Model):
    transaction_id = models.AutoField(primary_key=True)
    person_id = models.IntegerField(null=True, db_index=True)
    transaction_type_id = models.CharField(max_length=4, null=True, db_index=True)
    interface_id = models.CharField(max_length=4, null=True, db_index=True)
    date_recorded_paper = models.DateField(null=True)
    person_id_recorded_paper = models.IntegerField(max_length=4, null=True)
    timestamp_modified  = models.DateTimeField(auto_now_add=True)
    person_id_modified = models.IntegerField(null=True, db_index=True)
    
    class Meta:
        db_table = 'tbl_reg_persons_audit_trail'
       
class Forms(models.Model):
    form_id = models.CharField(max_length=32)
    form_type_id = models.CharField(max_length=4, null=True)
    form_subject_id = models.CharField(max_length=15, null=True)
    form_area_id = models.IntegerField(null=True)
    date_began = models.DateField(null=True)
    date_ended = models.DateField(null=True)
    date_filled_paper = models.DateField(null=True)
    person_id_filled_paper = models.IntegerField(null=True)
    org_unit_id_filled_paper = models.CharField(max_length=8, null=True)
    capture_site_id = models.IntegerField(null=True, db_index=True)
    timestamp_created = models.DateField(null=True)
    person_id_created = models.CharField(max_length=9, null=True)
    timestamp_updated = models.DateField(null=True)
    person_id_updated = models.CharField(max_length=9, null=True)
    is_void = models.BooleanField(default=False)
    
    class Meta:
        db_table = 'tbl_forms'

class AdminDownload(models.Model):
    capture_site_id = models.IntegerField(max_length=4, null=True, db_index=True)
    section_id = models.CharField(max_length=4, null=True)
    timestamp_started = models.DateField(null=True)
    timestamp_completed = models.DateField(null=True)
    number_records = models.IntegerField(null=True)
    success = models.BooleanField(default=False)
    
    class Meta:
        db_table = 'tbl_admin_download'

class AdminUploadForms(models.Model):
    form_id = models.CharField(max_length=32)
    timestamp_uploaded = models.DateField(null=True)
    
    class Meta:
        db_table = 'tbl_admin_upload_forms'

class ListQuestions(models.Model):
    
    class Meta:
        db_table = 'tbl_list_questions'
        
class ListAnswers(models.Model):
    
    class Meta:
        db_table = 'tbl_list_answers'
        
class ListAnswers(models.Model):
    
    class Meta:
        db_table = 'tbl_list_answers'
        
