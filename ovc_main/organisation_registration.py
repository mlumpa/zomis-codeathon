'''
Created on Sep 16, 2014

@author: MLumpa
'''
from models import RegOrgUnit, RegOrgUnitGdclsu, SetupGeorgraphy
from datetime import datetime
from models import RegOrgUnitContact, RegOrgUnitGeography, RegOrgUnitExternalID
from ovc_main.utils import lookup_field_dictionary as fielddictionary, fields_list_provider, audit_trail as logger
from ovc_main.utils.idgenerator import organisation_id_generator
from ovc_main.utils.geo_location import matches_for_display, search_org_in_location
from ovc_main.utils import auth_access_control_util
import traceback
import sys

from django.db.models import Q
import operator
from ovc_main.utils.fields_list_provider import get_org_from_id

__name__ = 'organisation'

org_permission_fields ={'other':
                                {'auth.update org contact': ['contact'],
                                  
                                  'auth.update org general':
                                        ['is_void', 'date_closed', 'date_operational','locationserviceprovided'],
                                  
                                  'auth.update org special':
                                        ['date_operational','legalregistrationtype','name', 'legalregistrationtype_name', 'registrationnumber']
                                },
                        'rel_org':
                                {
                                    'auth.update org contact rel': ['contact'],
                                    'auth.update org general rel':
                                                ['is_void', 'date_closed', 'date_operational']
                                }
                        }

class GeoLocation:
    geo_id = None
    geo_name = None
    geo_parent_id = None
    geo_level = None
    
    def __init__(self, geo_id):
        self.geo_id = geo_id

        geo_location = SetupGeorgraphy.objects.get(pk=geo_id)
        
        self.geo_name = geo_location.area_name
        self.geo_parent_id = geo_location.parent_area_id
        self.geo_level = geo_location.area_type_id
        
    def geo_dict(self):
        return {self.geo_id: self.geo_name}


class Contact:
        land_phone_number=None
        mobile_phone_number = None
        physical_address = None
        postal_address = None
        email_address = None
        
        def __init__(self, *args, **kwargs):
            if fielddictionary.contact_mobile_phone in kwargs:
                self.mobile_phone_number = kwargs[fielddictionary.contact_mobile_phone]
                
            if fielddictionary.contact_phone_number in kwargs:
                self.land_phone_number = kwargs[fielddictionary.contact_phone_number]
                
            if fielddictionary.contact_physical_address in kwargs:
                self.physical_address = kwargs[fielddictionary.contact_physical_address]
                
            if fielddictionary.contact_post_address in kwargs:
                self.postal_address = kwargs[fielddictionary.contact_post_address]
                
            if fielddictionary.contact_email_address in kwargs:
                self.email_address = kwargs[fielddictionary.contact_email_address]

        def __unicode__(self):
            return 'landphone:',self.land_phone_number, 'mobilephone:', self.mobile_phone_number,'physicaladdress:', self.physical_address, 'postaladdress:', self.postal_address, 'emailaddress:', self.email_address
        
        def get_contact_dict(self):
            return {fielddictionary.contact_mobile_phone: self.mobile_phone_number, 
                    fielddictionary.contact_phone_number: self.land_phone_number, 
                    fielddictionary.contact_physical_address: self.physical_address, 
                    fielddictionary.contact_post_address: self.postal_address,
                    fielddictionary.contact_email_address: self.email_address}
           
class Organisation:
    
    name = None
    registrationnumber = None
    legalregistrationtype =  None
    legalregistrationtype_name = None
    unit_type = None
    unit_type_name = None
    locationserviceprovided = {}
    contact = None
    org_id = None
    date_operational = None
    date_closed = None
    parent_organisation_id = None
    org_system_id = None
    locations_for_display = None
    parent_organisation_name = None


    
    def __init__(self, name,  organisationtype, contact,date_operational, 
                 registrationnumber=None,legalregistrationtype=None, parent_org_unit = None, 
                 org_id=None, date_closed=None, districts=None, wards=None, communities=None, 
                 org_system_id=None, parent_organisation_id=None, is_void=False):
        self.name = name
        self.registrationnumber = registrationnumber
        self.legalregistrationtype = legalregistrationtype
        if legalregistrationtype:
            self.legalregistrationtype_name = self.legalregistrationtype_text()
        self.contact = contact
        if not self.contact:
            self.contact = Contact()
        if org_id:
            self.org_id = org_id
        else:
            self.org_id = 'nxxxxx'
        self.parent_organisation_id = parent_org_unit
        self.date_operational = date_operational
        self.unit_type = organisationtype
        self.unit_type_name = self.unit_type_name()
        self.date_closed = date_closed
        self.parent_organisation_id = parent_organisation_id
        if self.parent_organisation_id:
            self.parent_organisation_name = RegOrgUnit.objects.get(pk=self.parent_organisation_id).unit_name
        
        self.org_system_id = org_system_id

        
        self.locationserviceprovided= {'districts':districts,
                                    'wards':wards,
                                    'communities':communities}
        
        
        
        
        _distrcits_wards = []
        _communities = None
        if wards:
            _distrcits_wards += wards
        if districts:
            _distrcits_wards += districts
            
        if _distrcits_wards:
            if self.locationserviceprovided['communities']:
                _communities = self.locationserviceprovided['communities']
            else:
                _communities = []
            self.locations_for_display = matches_for_display(_distrcits_wards, _communities)
        else:
            self.locations_for_display = []
        
        self.locations_unique_readable = []
        
        for loc in _distrcits_wards:
            self.locations_unique_readable.append(GeoLocation(loc).geo_name)
        
        if _communities:
            for comm in communities:
                self.locations_unique_readable.insert(0, RegOrgUnit.objects.get(pk=comm).unit_name)
        
        self.is_void = is_void
        self.date_closed = date_closed
        self.is_active = True
        if self.date_closed:
            self.is_active = False
        
    def __unicode__(self):
        return self.name
    
    def _get_locations_dictionary(self):
        return {'districts':self._districts,
            'wards': self._wards,
            'communities': self._communities}
    
    #locationserviceprovided = property(_get_locations_dictionary)
    
    def get_unit_type_description(self):
        return fields_list_provider.get_setup_field_description_from_item_id(self.unit_type)
    
    def unit_type_name(self):
        return fields_list_provider.get_setup_field_description_from_item_id(self.unit_type)
    
    def legalregistrationtype_text(self):
        return fields_list_provider.get_setup_field_description_from_item_id(self.legalregistrationtype)
    def get_locations_for_display(self):
        return self.locations_for_display

def create_org_object_from_page(cleanpagefields, user):
    print cleanpagefields, 'cleaned fields'
    organisation_name = None
    if cleanpagefields.has_key('org_name'):
        organisation_name = cleanpagefields['org_name']
    print organisation_name, 'this is the organistion name will pulled out' 
    org_system_id = None
    if cleanpagefields.has_key('org_system_id'):
        org_system_id = cleanpagefields['org_system_id']
        
    parent_unit = None
    if cleanpagefields.has_key('parent_unit'):
        parent_unit = cleanpagefields['parent_unit']
        
    legal_registration_type = None
    if cleanpagefields.has_key('legal_registration_type'):
        legal_registration_type = cleanpagefields['legal_registration_type']
    
    organisation_type = None
    if cleanpagefields.has_key('organisation_type'):
        organisation_type = cleanpagefields['organisation_type']
        
    date_org_setup = None
    if cleanpagefields.has_key('date_org_setup'):
        date_org_setup  = cleanpagefields['date_org_setup']

        
    legal_registratiom_number = None
    if cleanpagefields.has_key('legal_registratiom_number'):
        legal_registratiom_number = cleanpagefields['legal_registratiom_number']
        
    postal_address  = None
    if cleanpagefields.has_key('postal_address'):
        postal_address = cleanpagefields['postal_address']
    
    land_phone_number = None
    if cleanpagefields.has_key('land_phone_number'):
        land_phone_number = cleanpagefields['land_phone_number']
        
    physical_address = None
    if cleanpagefields.has_key('physical_address'):
        physical_address = cleanpagefields['physical_address']
        
    email_address = None
    if cleanpagefields.has_key('email_address'):
        email_address = cleanpagefields['email_address']
        
    mobile_phone_number = None
    if cleanpagefields.has_key('mobile_phone_number'):
        mobile_phone_number = cleanpagefields['mobile_phone_number']
        
    districts = None
    if cleanpagefields.has_key('districts'):
        if cleanpagefields['districts']:
            districts  = map(int, cleanpagefields['districts'])
    
    wards = None
    if cleanpagefields.has_key('wards'):
        if cleanpagefields['wards']:
            wards = map(int, cleanpagefields['wards'])
    
    communities = None
    if cleanpagefields.has_key('communities'):
        if cleanpagefields['communities']:
            communities = map(int, cleanpagefields['communities'])
            
    is_void = False
    if cleanpagefields.has_key('org_never_existed'):
        print 'org never existed'
        if cleanpagefields['org_never_existed']:
            is_void = cleanpagefields['org_never_existed']

    org_closed_not_functional = None
    if cleanpagefields.has_key('org_closed_not_functional'):
        print 'org is closed'
        org_closed_not_functional  = cleanpagefields['org_closed_not_functional']
     
    date_org_delinked = None     
    if org_closed_not_functional:
        if cleanpagefields.has_key('date_closed'):
            date_org_delinked  = cleanpagefields['date_closed']
    
    
    #select phone where colum = ''
    c_dict = {fielddictionary.contact_phone_number: land_phone_number, 
              fielddictionary.contact_mobile_phone: mobile_phone_number, 
              fielddictionary.contact_physical_address: physical_address,
              fielddictionary.contact_post_address: postal_address,
              fielddictionary.contact_email_address: email_address}
    contact = Contact(**c_dict)
    
    org = Organisation(name=organisation_name, legalregistrationtype=legal_registration_type, 
                       organisationtype=organisation_type, registrationnumber=legal_registratiom_number, 
                       date_operational=date_org_setup, contact=contact,
                       districts=districts, wards=wards, communities=communities, 
                       org_system_id=org_system_id, parent_organisation_id=parent_unit, 
                       is_void=is_void, date_closed= date_org_delinked)
    
    return org

def generate_unit_id(model_org):
    modelid = model_org.pk
    return organisation_id_generator(modelid)

def assign_org_unique_id(new_org):
    uniqueid = generate_unit_id(new_org)
    new_org.unit_id = uniqueid
    new_org.save()
    return uniqueid

def create_organisation(cleanpagefields, user):

    '''
    save first to reg org unit first
    '''
    org  = create_org_object_from_page(cleanpagefields, user)
    
    try:
        if org:
            print org.parent_organisation_id, 'in  fresh, parent org unit'
            new_org = save_basic_org(org)
            uniqueid = assign_org_unique_id(new_org)
            save_external_id(org, new_org)
            save_geo_location(org.locationserviceprovided, new_org)
            if org.contact:
                save_contact(org.contact, new_org)
            #if org.parent_organisation_id:
            #    save_organisation_hierarchy(org, new_org)
            #Log organisation registration here
            logger.log(__name__,new_org,fielddictionary.register_org_unit,fielddictionary.web_interface_id,person_electronic=user.pk)
        else:
            raise Exception('organisastion is a nonetype')

    except Exception as e:
        print e
        raise Exception('Organisation has some invalid data. Organisation Create failed.')
  
def update_organisation(cleanpagefields, user, changed_fields=None, is_selective_update=False):

    '''
    save first to reg org unit first
    '''
    print 'we are in update org'
    print is_selective_update
    org  = create_org_object_from_page(cleanpagefields, user)
    if is_selective_update:
        saved_org = load_org_from_id(org.org_system_id)
        fieldstoupdate = auth_access_control_util.filter_fields_for_display(user, org_permission_fields, organisation_id=org.org_system_id)
        print fieldstoupdate, 'fields needed for update in org registration'
        org = auth_access_control_util.update_object(object_to_update=saved_org, objects_with_updates=org, fields_to_update=fieldstoupdate)
    
    print vars(org)
    print vars(org.contact)
    #raise Exception('Mushashu generated errors')
    try:
        if org:
            new_org = update_basic_org(org)
            update_external_id(org, new_org)
            update_geo_location(org.locationserviceprovided, new_org)
            if org.contact:
                update_contact(org.contact, new_org)
            #if org.parent_organisation_id:
            #   update_organisation_hierarchy(org, new_org)
            #Log organisation update here
            logger.log(__name__,new_org,fielddictionary.update_org_unit,fielddictionary.web_interface_id,person_electronic=user.pk)
        else:
            raise Exception('organisastion is a nonetype')

    except Exception as e:
        print e
        raise Exception('Organisation has some invalid data. Organisation Create failed.')
 
def update_basic_org(org):
    morg = None
    try:
        isgu = False
        if org.unit_type == fielddictionary.gdclsu_text:
            isgu = True
        morg = RegOrgUnit.objects.get(pk=org.org_system_id)
        #org.unit_id=org.org_id
        morg.unit_name=org.name
        morg.is_gdclsu=isgu
        morg.date_operational=org.date_operational
        morg.date_closed=org.date_closed
        morg.unit_type_id=org.unit_type
        morg.is_void = org.is_void
        morg.parent_org_unit_id=org.parent_organisation_id
        
        morg.save()
    
    except Exception as e:
        print e
        raise Exception('0rganisatnion update Failed')
    return morg

def save_basic_org(org): 
    morg = None   
    try:
        isgu = False
        print org.parent_organisation_id, 'in  create, parent org unit'
        if org.unit_type == fielddictionary.gdclsu_text:
            isgu = True
        morg = RegOrgUnit.objects.create(unit_id=org.org_id, unit_name=org.name,is_gdclsu=isgu, 
                                         date_operational=org.date_operational,unit_type_id=org.unit_type,
                                         parent_org_unit_id=org.parent_organisation_id)
            
    
    except Exception as e:
        print e
        print org.org_id
        print org.name
        print isgu
        print org.unit_type
        traceback.print_exc()
        raise Exception('0rganisatnion Creation Failed')
    return morg
     
def save_contact(contact, org):

    
    try:
        for contactkey in contact.get_contact_dict().keys():
            if contact.get_contact_dict()[contactkey]:
                RegOrgUnitContact.objects.get_or_create(organisation = org, contact_detail=contact.get_contact_dict()[contactkey], contact_detail_type_id=contactkey, is_void=False)
        #void the ones that are there already
        RegOrgUnitContact.objects.filter(organisation = org, is_void=False).exclude(contact_detail__in=contact.get_contact_dict().values()).update(is_void=True)
    except Exception as e:
        print e
        raise Exception('Org Contact creation failed')
    
def update_contact(contact, org):

    
    try:
        RegOrgUnitContact.objects.filter(organisation=org, 
                                         is_void=False).exclude(contact_detail__in=contact.get_contact_dict().values()).update(is_void=True)
        for contactkey in contact.get_contact_dict().keys():
            if not contact.get_contact_dict()[contactkey]:
                continue
            if RegOrgUnitContact.objects.filter(organisation=org, 
                                                contact_detail=contact.get_contact_dict()[contactkey], contact_detail_type_id=contactkey, is_void=False).count() > 0:
                #do nothing
                continue
            elif RegOrgUnitContact.objects.filter(organisation=org, contact_detail_type_id=contactkey, is_void=False).count() > 0:
                #so, the value changed, update the records
                RegOrgUnitContact.objects.filter(organisation=org, contact_detail_type_id=contactkey, is_void=False).update(contact_detail=contact.get_contact_dict()[contactkey])
            else:
                RegOrgUnitContact.objects.create(organisation = org, contact_detail=contact.get_contact_dict()[contactkey], contact_detail_type_id=contactkey)
    except Exception as e:
        print e
        raise Exception('Org Contact creation failed')

def save_external_id(org, model_org):
    try:
        RegOrgUnitExternalID.objects.filter(organisation=model_org, is_void=False).exclude(identifier_type_id=org.legalregistrationtype, identifier_value=org.registrationnumber).update(is_void=True)
        if org.legalregistrationtype:
            RegOrgUnitExternalID.objects.get_or_create(organisation=model_org, identifier_type_id=org.legalregistrationtype, identifier_value=org.registrationnumber, is_void=False)
    except Exception as e:
        print e
        print 'failed to create external id'
        
def update_external_id(org, model_org):
    print 'updating external ids'
    try:
        RegOrgUnitExternalID.objects.filter(organisation=model_org, is_void=False).exclude(identifier_type_id=org.legalregistrationtype, identifier_value=org.registrationnumber).update(is_void=True)
        if org.legalregistrationtype:
            if RegOrgUnitExternalID.objects.filter(organisation=model_org, identifier_type_id=org.legalregistrationtype, identifier_value=org.registrationnumber, is_void=False).count() > 0:
                #do nothing
                pass
            else:
                RegOrgUnitExternalID.objects.get_or_create(organisation=model_org, identifier_type_id=org.legalregistrationtype, identifier_value=org.registrationnumber)
        else:
            RegOrgUnitExternalID.objects.filter(organisation=model_org, is_void=False).update(is_void=True)
    except Exception as e:
        print e
        print 'failed to create external id'

def save_geo_location(locations, new_org):
    try:
        for location in locations:
            if not locations[location]:
                continue
            if location=='communities':
                communities = locations[location]
                for community in communities:
                    if new_org.date_operational:
                        RegOrgUnitGdclsu.objects.get_or_create(organisation=new_org, gdclsu_id=community, date_linked=new_org.date_operational, date_delinked=new_org.date_closed, is_void=False)
                    else:
                        RegOrgUnitGdclsu.objects.get_or_create(organisation=new_org, gdclsu_id=community, date_linked=datetime.now().date(), date_delinked=new_org.date_closed, is_void=False)
            else:
                tmp_date_operational = new_org.date_operational
                if not tmp_date_operational:
                    tmp_date_operational = datetime.now().date()
                for selected_location in locations[location]:
                    RegOrgUnitGeography.objects.get_or_create(organisation=new_org, area_id=selected_location, 
                                                       date_linked=tmp_date_operational, date_delinked=new_org.date_closed, is_void=False)
    except Exception as e:
        print e
        raise Exception('failed to save to geo locations')
    
def update_geo_location(locations, new_org):
    '''
    location is a dictionary of locations with the key being the type of location.
    e.g., {district:[list of district ids], constituency:[list of constituencies ] etc}
    '''
    
    tolocationstoupdate = []
    try:
        for location_level in locations:#get location types
            if not locations[location_level]:
                continue
            if 'communities' == location_level:
                RegOrgUnitGdclsu.objects.filter(organisation=new_org, is_void=False).exclude(gdclsu_id__in=locations[location_level]).update(date_delinked=datetime.now().date(), is_void=True)
                communities = locations[location_level]
                for community in communities:
                    org_geo, created = RegOrgUnitGdclsu.objects.get_or_create(organisation=new_org, gdclsu_id=community, is_void=False)
                    if created:
                        org_geo.date_linked =  date_linked=datetime.now().date()
                        org_geo.save()
                continue
            tolocationstoupdate += locations[location_level]
        
        RegOrgUnitGeography.objects.filter(organisation=new_org, is_void=False).exclude(area_id__in=tolocationstoupdate).update(is_void=True, date_delinked=datetime.now().date())              
        for locationid in tolocationstoupdate:#get specifict locations
            org_geo, created = RegOrgUnitGeography.objects.get_or_create(organisation=new_org, area_id=locationid, is_void=False)
            if created:
                org_geo.date_linked = datetime.now().date()
                org_geo.save()
    except Exception as e:
        print e
        traceback.print_exc()
        raise Exception('failed to update to geo locations')


def search_orgs(tokens, search_location=True, org_type=None, is_active=True):
    result = []
    q_list = []
    if tokens:
        try:
            for token in tokens:
                #http://www.michelepasin.org/blog/2010/07/20/the-power-of-djangos-q-objects/
                q_list.append(Q(unit_name__icontains=token.lower()))
                q_list.append(Q(unit_id__icontains=token.lower()))
            
            if org_type and tokens:
                tmp_result = RegOrgUnit.objects.filter(unit_type_id=org_type, is_void=False).filter(reduce(operator.or_, q_list))
                print tmp_result, 'org type and tokens supplied'
            else:
                print org_type, 'in else'
                tmp_result = RegOrgUnit.objects.filter(reduce(operator.or_, q_list), is_void=False)
            
            print tmp_result, 'we had values'
            for org in tmp_result:
                if org.is_active:
                    result.append(org) 
                
            print result, 'there were more results'
            
            if search_location:
                org_ids = search_org_in_location(tokens)
                print org_ids, 'our ids from geo'
                if org_ids:
                    orgstofetch = list(set(org_ids) - set([org.pk for org in result]))
                    
                    if org_type and orgstofetch:
                        orgs_by_geo= RegOrgUnit.objects.filter(unit_type_id=org_type, is_void=False).filter(pk__in=orgstofetch)
                        print orgs_by_geo, 'geo results with type'
                    else:
                        orgs_by_geo= RegOrgUnit.objects.filter(pk__in=orgstofetch, is_void=False)
                        print orgs_by_geo, 'geo results without type'
                    
                    for org in orgs_by_geo:
                        if org.is_active:
                            result.append(org)
                
        except Exception as e:
            print e
            raise Exception('Org search failed')
   
    return result

def get_org_list(tokens=None,getJSON=False, org_type=None, search_all_orgs = False):
    orgs = []
    
    matching_geo_org_ids = []

    modelorgs = search_orgs(tokens, org_type=org_type)
    print modelorgs, 'search reults from search'
    if not modelorgs:
        print org_type, 'our org type'
        if org_type and not tokens:
            print 'we are here, in orgtype and tokens'
            modelorgs = RegOrgUnit.objects.filter(unit_type_id__iexact=org_type, is_void=False)
        elif not org_type and not tokens:
            modelorgs = RegOrgUnit.objects.all()

    tmp_org_ids = []
    for org in modelorgs:
        try:
            #if search all orgs is true, then return orgs that are not active as well
            if not org.is_active and not search_all_orgs:
                continue
            org = load_org_from_id(org.pk)
            if getJSON:
                org = org_json(org)
                orgs.append(org)
            else:
                orgs.append(org)
        except Exception as e:
            print e
            traceback.print_exc()
            raise Exception('retrieving organisations failed')
    return orgs

def org_json(organisation):
    parent_org = organisation.parent_organisation_name
    if not organisation.parent_organisation_name:
        parent_org = 'None'
    org = {'org_system_id': organisation.org_system_id,
                 'org_id': organisation.org_id,
                'org_name': organisation.name,
                'unit_type_name': organisation.unit_type_name,
                'parent_organisation_name': parent_org,
                'location':'; '.join(organisation.locations_unique_readable),
                'is_active': organisation.is_active,}
    return org

def load_org_from_id(org_pk):
    if RegOrgUnit.objects.filter(pk=org_pk).count() > 0:
        try:
            m_org = RegOrgUnit.objects.get(pk=org_pk)
            
            contact = None
            geos = None
            legal_reg_type = None
            legal_reg_type_value = None
            ward=None
            district = None
            communities = None
            if RegOrgUnitContact.objects.filter(organisation=m_org, is_void=False).count() > 0:
                m_org_contact = RegOrgUnitContact.objects.filter(organisation=m_org, is_void=False)
                contactdict = {}
                for mcontact in m_org_contact:
                    contactdict[mcontact.contact_detail_type_id] = mcontact.contact_detail
                contact = Contact(**contactdict)
                
           
            if RegOrgUnitExternalID.objects.filter(organisation=m_org, is_void=False).count() > 0:
                legal_reg_type = RegOrgUnitExternalID.objects.get(organisation=m_org, is_void=False).identifier_type_id
                legal_reg_type_value = RegOrgUnitExternalID.objects.get(organisation=m_org, is_void=False).identifier_value
            
            geos = {}
            if RegOrgUnitGeography.objects.filter(organisation=m_org, is_void=False).count() > 0:
                m_org_geo = RegOrgUnitGeography.objects.filter(organisation=m_org, is_void=False)
                
                for geo in m_org_geo:
                    areainfo = SetupGeorgraphy.objects.get(area_id=geo.area_id, is_void=False)
                    if areainfo.area_type_id in geos:
                        geos[areainfo.area_type_id].append(areainfo.area_id)
                    else:
                        geos[areainfo.area_type_id] = [areainfo.area_id]
            if geos and 'district' in geos:
                district = geos['district']
            if geos and 'ward' in geos:
                ward = geos['ward']
            
            communties = {}
            if RegOrgUnitGdclsu.objects.filter(organisation=m_org, is_void=False).count() > 0:
                communities = RegOrgUnitGdclsu.objects.filter(organisation=m_org, is_void=False).values_list('gdclsu_id', flat=True)
            
            org = Organisation(name=m_org.unit_name, legalregistrationtype=legal_reg_type, 
                           organisationtype=m_org.unit_type_id, registrationnumber=legal_reg_type_value, 
                           date_operational=m_org.date_operational, contact=contact,
                           districts=district, wards=ward, communities=communities, org_id=m_org.unit_id, 
                           org_system_id=m_org.pk, parent_organisation_id=m_org.parent_org_unit_id, is_void=m_org.is_void,
                           date_closed= m_org.date_closed)
            
            return org
        except Exception as e:
            print m_org, 'our models'
            print e
            print contact
            print m_org.unit_name
            print m_org.date_closed
            traceback.print_exc()
            raise Exception('something whent wrong when retrieving an organisation')
    
    else:
        raise Exception('Organisation with the ID passsed does not exists')
    
def get_cwacs_in_ward(wardid):
    orgs_to_return = []
    if RegOrgUnit.objects.filter(is_gdclsu=True, is_void=False).count() > 0:
        cwacs = RegOrgUnit.objects.filter(is_gdclsu=True, is_void=False)
        
        print cwacs, 'print cwacs here'
        for cwac in cwacs:
            print cwac, 'here a cwac'
            org = load_org_from_id(cwac.pk)
            print org.locationserviceprovided['wards'], 'available wards'
            if 'wards' in org.locationserviceprovided:
                wards = org.locationserviceprovided['wards']
                wardid = int(wardid)
                if wardid in wards:
                    orgs_to_return.append(org)
    
    return orgs_to_return
        
    
    
    
    
         
        
