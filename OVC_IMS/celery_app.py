
from __future__ import absolute_import
import os
import sys
from celery import Celery
from django.conf import settings
filedir = os.path.dirname(__file__)
main_proj = os.path.join('../', filedir)
sys.path.append(main_proj)
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'OVC_IMS.settings')
app = Celery('OVC_IMS')
app.config_from_object('django.conf:settings')
app.autodiscover_tasks(lambda: settings.INSTALLED_APPS)
