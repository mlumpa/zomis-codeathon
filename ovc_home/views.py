from django.shortcuts import render
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse
from ovc_auth.models import OVCRole
from ovc_auth.allocate_form_manager import prepare_form_fields, process_allocate_values
from ovc_auth.change_password_form import ChangePasswordForm
from ovc_auth import user_manager

# Create your views here.

def ovc_login(request):
    user = None
    failed_message = ''
    if request.POST: 
        username = request.POST['username']
        password = request.POST['password']
        
        is_valid, user = is_user_valid(username, password)
        
        if is_valid:
            login(request, user)
            return HttpResponseRedirect(reverse(home_dashboard))
        
        failed_message = 'Credentials used not correct. Try again'
                
    
    if request.user.is_authenticated():
        return HttpResponseRedirect(reverse(home_dashboard))
    
    return render(request, 'login.html', {'message':failed_message})

def is_user_valid(username, password):
    user = authenticate(username=username, password=password)
    if user and user.is_active:
        return True, user
    return False, user
    
@login_required(login_url='/')
def home_dashboard(request):
    return render(request, 'ovc_main/home_dashboard.html')


def ovc_logout(request):
    logout(request)
    return render(request, 'login.html')

@login_required(login_url='/')
def change_password(request):
    form = ChangePasswordForm(request.user)
    print request.POST, 'CHECKING OUR PASSWORD STUFF'
    if request.POST:
        form = ChangePasswordForm(request.user, request.POST) 
        if form.is_valid():
            data = form.cleaned_data
            new_password = form.cleaned_data.get('new_password')
            print new_password, 'our new password'
            user_manager.set_password(request.user, new_password)
            return HttpResponseRedirect('/home/')
        
    return render(request, 'ovc_main/change_password.html', {'form':form})

@login_required(login_url='/')
def allocate_role(request):
    
    user = request.user
    if request.POST:
        print request.POST,'stuff to print in post'
        workforce_id = request.POST['workforceid']
        process_allocate_values(request.POST.lists(), user, workforce_id)
        #do the processing here and return to home.
        return HttpResponseRedirect('/workforce/wfcsearch/')    
    
    if request.GET:
        workforce_id = request.GET['workforceid']
        rowfielddetails, workforce = prepare_form_fields(user, workforce_id)
        print workforce, 'this is suppose to be our workfroce model id'
    #return render(request, 'ovc_main/allocate_role.html', {'roles':roles, 'districts': districts_without_children, 'organisations':orgs, 'workforce':workforce,})
    return render(request, 'ovc_main/allocate_role.html', {'rowfielddetails':rowfielddetails, 'workforce':workforce, 'workforceid':workforce.id_int})