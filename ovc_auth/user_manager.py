from ovc_auth.models import OVCRole, AppUser, OVCUserRoleGeoOrg
from ovc_main.models import RegPerson

default_password = 'default'

def create_user_from_workforce_id(workforceid):
    workforce_model = RegPerson.objects.get(pk=workforceid)
    return create_user_from_workforce_model(workforce_model)

def create_user_from_workforce_model(workforce_model, password=None):
    
    workforce_id = workforce_model.workforce_id
    national_id = workforce_model.national_id
    first_name = workforce_model.first_name
    last_name = workforce_model.surname
    user = None
    if not password:
        password=default_password
    
    if AppUser.objects.filter(workforce_id = workforce_id).count() > 0:
        user = AppUser.objects.get(workforce_id=workforce_id)
        user.reg_person =  workforce_model
    else:
        if workforce_id:
            user = AppUser.objects.create_user(workforce_id=workforce_id, national_id=national_id, 
                                               first_name = first_name, last_name=last_name, password=password)
        else:
            user = AppUser.objects.create_user(workforce_id=str(workforce_model.pk), national_id=national_id, 
                                              first_name = first_name, last_name=last_name, password=password)
    
    user.reg_person = workforce_model
    
    user.save()
    
    return user

def allocate_standard_log_in(workforce_model, password=None):

    print 'allocate role'
    user =  get_or_create_user_from_model(workforce_model) 
    print user, 'in allocate standard logged in'  
    _assign_standard_login_role(user)
    
    return user
def _assign_standard_login_role(user):
    assign_role(user=user, rolename='Standard logged in')
    
def get_or_create_user_from_model(workforce_model, password=None):
    
    workforce_id = workforce_model.workforce_id
    national_id = workforce_model.national_id
    user = None
    try:
        if workforce_id:
            user = AppUser.objects.get(workforce_id=workforce_id)
        elif national_id:
            user = AppUser.objects.get(national_id=national_id)
        elif workforce_model:
            user = AppUser.objects.get(reg_person__pk=workforce_model.pk)
    except AppUser.DoesNotExist:
        user = create_user_from_workforce_model(workforce_model, password)
    return user

def allocate_registration_assistant_role(workforce_model, organisation_id=None, password=None):
    
    user =  get_or_create_user_from_model(workforce_model, password)      
    _assign_registration_assistant_role(user, organisation_id)
    
    return user
    
def _assign_registration_assistant_role(user, organisation_id):
    assign_role(user=user, rolename='Registration assistant', organisationid=organisation_id)
    
def assign_role(user, rolename, organisationid=None, area_id=None):
    try:
        role = OVCRole.objects.get(group_name=rolename)
        role.user_set.add(user)
        if organisationid or area_id:
            org=None
            area= None
            from ovc_main.models import RegOrgUnit, RegPersonsGeo
            if organisationid:
                
                org = RegOrgUnit.objects.get(pk=organisationid)
            if area_id:
                area = RegPersonsGeo.objects.get(area_id__exact=area_id, person=user.reg_person)
            createdresult = OVCUserRoleGeoOrg.objects.get_or_create(user=user, group=role, org_unit=org, area=area)
            
    except Exception as e:
        print e
        raise Exception('Role %s does not exist %s' % (rolename,e))
    
def get_user_group(user):
    return user.groups.all()

def get_ovc_user_roles(user):
    return OVCRole.objects.filter(group_ptr__in=user.groups.all())

def get_user_role_geo_org(user):
    ovc_roles_with_geo_and_org_info = OVCUserRoleGeoOrg.objects.filter(user=user,void=False)
    #ovc_roles_with_geo_and_org_info = OVCUserRoleGeoOrg.objects.filter(group__in=user.groups.all())
    return ovc_roles_with_geo_and_org_info

def reset_ovc_user_role_geo_org(user):
    OVCUserRoleGeoOrg.objects.filter(user=user).delete()
    
def remove_role(user, group, organisation_id=None, area_id=None):
    if organisation_id or area_id:
        OVCUserRoleGeoOrg.objects.filter(user=user, group=group, org_unit_id__pk=organisation_id, area__area_id=area_id).delete()
    
    #if this role belongs to this user to multple locations or organisations
    #dont delete the role from the user.
    if OVCUserRoleGeoOrg.objects.filter(user=user, group=group).count() > 0:
        pass
    else:
        user.groups.remove(group)
    
def reset_password(user):
    user.set_password(default_password)
    user.save()
    
def set_password(user, new_password):
    user.set_password(new_password)
    user.save()
