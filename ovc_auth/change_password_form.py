from django import forms
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Fieldset, ButtonHolder, Submit, Button, HTML, MultiField, Div, Field
from crispy_forms.bootstrap import FormActions, AppendedText
from ovc_main.models import SetupList, SetupGeorgraphy
from ovc_main.utils.fields_list_provider import get_org_list
from functools import partial
from ovc_main.utils.geo_location import get_communities_in_ward
DateInput = partial(forms.DateInput, {'class': 'datepicker'})
import datetime
from django.core.exceptions import ValidationError

class ChangePasswordForm(forms.Form):
    old_password = forms.CharField(widget=forms.PasswordInput())
    new_password = forms.CharField(widget=forms.PasswordInput())
    repeat_password = forms.CharField(widget=forms.PasswordInput())
    
    
    def __init__(self,user, *args, **kwargs):
        self.user = user
        super(ChangePasswordForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_class = 'form-horizontal'
        self.helper.label_class = 'col-md-3'
        self.helper.field_class = 'col-md-6'
        self.helper.form_tag = False

        self.helper.layout = Layout(
                                    'old_password', 
                                    'new_password', 
                                   'repeat_password'
                                    )
    def clean(self):
        print 'passed through clean method'
        old_password = self.cleaned_data.get('old_password')
        if not self.user.check_password(old_password):
            raise ValidationError('Your entered password does not match your old password!')
        
        new_password = self.cleaned_data.get('new_password')
        repeat_password = self.cleaned_data.get('repeat_password')
        
        if new_password != repeat_password:
            raise ValidationError('You new passwords do not match!')
        
        return self.cleaned_data