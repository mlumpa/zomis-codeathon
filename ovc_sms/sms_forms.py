from simplesmsforms.smsform import SMSForm
from simplesmsforms.smsform_fields import PrefixField, SingleChoiceField, DateField

class RECSForm(SMSForm):
	keyword = "RECS"
	LOCATION_CHOICES = ("EHOM", "EWRK", "EELS")

	workforceID = PrefixField(special_key=["w", "wnrc"], name="workforceID")
	password = PrefixField(special_key=["x"], name="password", required=False)
	beneficiaryID = PrefixField(special_key=["b", "bnrc", "bbcn"], name="beneficiaryID")
	location = SingleChoiceField(special_key=["loc"], name="location", choices=LOCATION_CHOICES)
	orgID = PrefixField(special_key=["u"], name="organisationID", required=False)
	ward =  PrefixField(special_key=["lgeo"], name="ward", required=False)
	serviceDate = DateField(special_key=["dt"], name="service_date", required=False)
	servicesProvided = PrefixField(special_key=["sp"], name="services_provided")
	referralProvided = PrefixField(special_key=["refp"], name="referral_provided")
	adverseConditions = PrefixField(special_key=["ac"], name="adverse_conditions")
