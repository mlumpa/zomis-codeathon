from django.conf.urls import patterns, include, url


urlpatterns = patterns('',
    ('accounts/', include('rapidsms.urls.login_logout')),
    # RapidSMS contrib app URLs
    (r'^httptester/', include('rapidsms.contrib.httptester.urls')),
    #(r'^locations/', include('rapidsms.contrib.locations.urls')),
    (r'^messagelog/', include('rapidsms.contrib.messagelog.urls')),
    (r'^messaging/', include('rapidsms.contrib.messaging.urls')),
    (r'^registration/', include('rapidsms.contrib.registration.urls')),
    # Third party URLs
    (r'^selectable/', include('selectable.urls'))
    )
